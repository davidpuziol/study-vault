# UI

Vamos dar um giro pela interface grafica do vault

lembrando de liberar o security group da ec2 que roda o vault na porta 8200

>É necessaŕio liberar a porta 8200 também do no security group da ec2
![sg](../pics/secgroup.png)

E vamos acessar o mesmo vault address exportado

export VAULT_ADDR='<http://ec2-18-236-71-160.us-west-2.compute.amazonaws.com:8200>'

Varios são os métodos de autenticacão

![auth](../pics/authvault.png)

Para autenticar utilize nesse momento o root token que criamos `vaulvault` ou aquele que esta sendo mostrado na saída do ssh 1 que criamos para levantar o server.

Observer o status `unsealed` que temos por conta do -dev que do server.

Vamos fazer um overview pelo secrets rapido. Tudo que podemos fazer via cli podemos fazer via ui

![gifsecret](../gifs/secretsui.gif)

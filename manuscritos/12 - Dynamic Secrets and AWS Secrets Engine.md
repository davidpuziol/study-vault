# Dynamic Secrets e AWS Secret Engine

<https://developer.hashicorp.com/vault/docs/secrets/aws>

Para acessar da sua máquina pessoal, aponte para o ip do seu vault e use o root token por enquanto já que não temos nenhum user configurado.

O aws secrets engine será a função do vault de se comunicar com o IAM da AWS.
O Dynamic é uma habilidade do vault de gerar um secret dinâmicamente e revogar depois de um tempo pré configurado chamado `lease time` que passamos na configuração do vault (`vault.hcl`).

```bash
export VAULT_ADDR='http://54.212.6.252:8200'  
export VAULT_TOKEN="hvs.oBw0OfJtBJ0cVw8LodpBVfc2"

❯ vault secrets enable aws
Success! Enabled the aws secrets engine at: aws/

❯ vault secrets list
Path          Type         Accessor              Description
----          ----         --------              -----------
aws/          aws          aws_3a5e48b3          n/a
cubbyhole/    cubbyhole    cubbyhole_7c7a7f10    per-token private secret storage
identity/     identity     identity_0dd9718b     identity store
sys/          system       system_d63da29b       system endpoints used for control, policy and debugging
```

Precisamos configurar uma conta para o vault na aws que tenha permissão para criar outras contas e dar a elas permissões.

No exemplo abaixo podemos criar uma conta admin para o vault, ou podemos também adicionar um user com uma policitica específica.

Para admin
![vaultiam](../pics/vaultiam.png)
![vaultiam](../pics/vaultiamadmin.png)

Utiliza essa password e secret para configurar o aws secret

```bash
vault write aws/config/root access_key=AKIA4VFT7SYOZOFTBRNL secret_key=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 region=us-west-2 
Success! Data written to: aws/config/root
```

Um coisa importante  é definir uma política específica para o vault ao invés de dar a ele um admin. Use a política abaixo caso não queira dar um admin.

```json
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "iam:AttachUserPolicy",
        "iam:CreateAccessKey",
        "iam:CreateUser",
        "iam:DeleteAccessKey",
        "iam:DeleteUser",
        "iam:DeleteUserPolicy",
        "iam:DetachUserPolicy",
        "iam:GetUser",
        "iam:ListAccessKeys",
        "iam:ListAttachedUserPolicies",
        "iam:ListGroupsForUser",
        "iam:ListUserPolicies",
        "iam:PutUserPolicy",
        "iam:AddUserToGroup",
        "iam:RemoveUserFromGroup"
      ],
      "Resource": ["arn:aws:iam::ACCOUNT-ID-WITHOUT-HYPHENS:user/vault-*"]
    }
  ]
}
```

Agora é necessário criar algumas roles para que o vault vincule o iam que ele criará na aws com essas roles. Essas roles concedem permissões espefícias, por isso é possível ter várias roles diferentes e depois vincular uma política no vault para que determinada usuário possa ou não pegar essa role.

Crie uma políce em um arquivo que ficará mais fácil aplicar. Use a própria aws para criar uma política caso precise e retire o json de lá. Aqui um exemplo que vamos usar: Essa é uma policy que dá todo acesso ao ec2. Salve ela em um arquivo que isará usar para aplicar.

```json
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "ec2:*"
            ],
            "Resource": [
                "*"
            ]
        }
    ]
}
```

```bash
❯ vault write aws/roles/ec2-role credential_type=iam_user policy_document=@./resources/ec2-full-policy.json 
Success! Data written to: aws/roles/ec2-role

❯ vault read aws/roles/ec2-role
Key                         Value
---                         -----
credential_type             iam_user
default_sts_ttl             0s
iam_groups                  <nil>
iam_tags                    <nil>
max_sts_ttl                 0s
permissions_boundary_arn    n/a
policy_arns                 <nil>
policy_document             {"Version":"2012-10-17","Statement":[{"Effect":"Allow","Action":["ec2:*"],"Resource":["*"]}]}
role_arns                   <nil>
user_path                   n/a
```

Para gerar uma chave por exemplo utilizando essas credenciais. Com essa chave será possível fazer tudo que a política deu permissão.

```bash
❯ vault read aws/creds/ec2-role
Key                Value
---                -----
lease_id           aws/creds/ec2-role/AGGEJo6vBMaSN3H2hFcB6s3g
lease_duration     6h
lease_renewable    true
access_key         AKIA4VFT7SYOQZECE2FX
secret_key         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
security_token     <nil>

#Vamos gerar mais uma
❯ vault read aws/creds/ec2-role
Key                Value
---                -----
lease_id           aws/creds/ec2-role/KpXLYUlR2o2tuL8NPnTBIeVf
lease_duration     6h
lease_renewable    true
access_key         AKIA4VFT7SYOWNXMHCQZ
secret_key         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
security_token     <nil>
```

Do mesmo modo é possível gerar pela gui

![generate](../pics/credsgenerate.png)

Também podemos conferir as credencials criadas

![lease](../pics/generatedlease.png)

Se quisesse por exemplo passar uma policy que já esteja criada na conta ou uma police de aws

```bash
vault write aws/roles/my-other-role \
    policy_arns=arn:aws:iam::aws:policy/AmazonEC2ReadOnlyAccess,arn:aws:iam::aws:policy/IAMReadOnlyAccess \
    iam_groups=group1,group2 \
    credential_type=iam_user \
    policy_document=-<<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": "ec2:*",
      "Resource": "*"
    }
  ]
}
EOF
```

Para implementar essa chamada via api <https://developer.hashicorp.com/vault/api-docs/secret/aws>

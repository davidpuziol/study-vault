# Conceitos

Aqui uma arquitetura mais detalhadas sobre o vault

![Vault](../pics/archha.png)

Analisando a figura podemos entender que envolta do core que é de fato a inteligencia do vault temos alguns outros recursos.

A primeira coisa a se entender que existem muito plugins de autenticacão que somente servirão para identirficar quem esta fazendo a chamadas para poder aplicar as regras.

Depois disspo podemos observar que um sistema de auditoria (na parte debaio) irá logar em algum servidor de log que poderia ou não estar em alta disponibilidade, mas deveria, por isso esta pontilhado.

Na parte de cima temos os storage backend que será onde as chaves verdadeiras e fixas são guardadas. Podemo utilizar várias ferramentas para isso, mostrando a flexxibilidade do vault. Uma ponto de atencão é que se esse banco de dados não for acessado, teremos um prolema grave. Se o Vault tem a proposta de centralizar tudo, então não pode parar, pq se parar vai cair tudo. Por isso é recomendável que para esse sistema tenha uma auta dispobilidade.

Por útlimo e na parte trazeira, temos a parte do vault que cuida dos secrets dinâmicos, que são gerados pelo Vault temporáriamente para que exista uma rotatividade de chave. Esse módulo também possui diferentes integracões como no caso mostrado o aws para criar uma role temporária para algum cliente.

Não menos importante, do que adianta o banco de dados ter alta disponiilidade se o vault não tiver? Por isso é recomendado que execute pelo menos 3 instancia do vault.

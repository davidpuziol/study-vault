# Study Vault

<img src="./pics/vault.png" alt="drawing" width="250"/>

Está na hora de deixar de colocar suas secrets direto no código e ter um server dedicado para cuidar desse problema. É ai que entra o [vault](https://www.vaultproject.io/).

Como sempre a [documentacao oficial](https://developer.hashicorp.com/vault/docs) é o melhor lugar para consulta.

Essa é uma ferramenta da Hashicop e tem uma documentacao muito boa.

O Vault é open source, desenvolvido em golang e o código esta no github <https://github.com/hashicorp/vault>

## O que é o Vault?

Um segredo é qualquer coisa que você deseja controlar rigidamente o acesso.

- Usernames (autenticacão)
- DB Credencials (autenticacão)
- Api Token (autenticacão)
- tls certificados (Autorizacão)

>Um segredo pode prover autenticacão ou autorizacão para um sistema.

Um sistema moderno requer acesso a uma infinidade de segredos, incluindo credenciais de banco de dados, chaves de API para serviços externos, credenciais para comunicação de arquitetura orientada a serviços etc.

Pode ser difícil entender quem está acessando quais segredos, especialmente porque isso pode ser de plataforma específica. Adicionar rolagem de chaves, armazenamento seguro e logs de auditoria detalhados é quase impossível sem uma solução personalizada. É aqui que entra o Vault.

- O Vault fornece serviços de criptografia controlados por métodos de autenticação e autorização.

- Usando a interface do usuário do Vault, CLI ou API HTTP, o acesso a segredos e outros dados confidenciais pode ser armazenado e gerenciado com segurança, rigidamente controlado (restrito) e auditável.

- O Vault valida e autoriza clientes (usuários, máquinas, aplicativos) antes de fornecer acesso a segredos ou dados confidenciais armazenados.

## Por que usar o Vault?

- Evitar credenciais espalhadas pela organizacão e armazenadas em textos simples, dentro do código fonte, arquivos de configuracão e outros locais.
  - Facilidade de expansão
  - Saber quem tem acesso e autorizacão para o quê
  - Diminuir o potencial de ataques mal-intencionados tanto externo quanto interno.
- Centralizacão das credenciais reduz a exposicão indesejadas de credenciais.
- Auditoria de quem usou acessou a credencial, mantendo um histórico das acões dos clientes.
- Gerenciar segredos independente da cloud.
- Gerenciar acesso aos endpoints da aplicacão
- Rrevogar o acesso rápidamente e em massa
- Segredos dinâmicos
  - Muitas devs acabam logando os segredos, e este podem ser expostos pelo logs, as vezes o próprio stack trace do erro mostra o valor.
- Encriptacão dos segredos
- Rotatividade de credenciais de banco de dados
- Criar certificados sob demanda
- Integra tudo a maioria das tecnologias utilizadas

Os principais recursos do Vault são:

- `Armazenamento de segredo seguro` : segredos de chave/valor arbitrários podem ser armazenados no Vault. O Vault criptografa esses segredos antes de gravá-los no armazenamento persistente, portanto, obter acesso ao armazenamento bruto não é suficiente para acessar seus segredos. O Vault pode gravar em disco, Consul e muito mais.

- `Segredos dinâmicos` : o Vault pode gerar segredos sob demanda para alguns sistemas, como bancos de dados AWS ou SQL. Por exemplo, quando um aplicativo precisa acessar um bucket S3, ele solicita credenciais ao Vault, e o Vault gera um par de chaves AWS com permissões válidas sob demanda. Depois de criar esses segredos dinâmicos, o Vault também os revogará automaticamente após o término da concessão.

- `Criptografia de dados` : o Vault pode criptografar e descriptografar dados sem armazená-los. Isso permite que as equipes de segurança definam parâmetros de criptografia e que os desenvolvedores armazenem dados criptografados em um local como um banco de dados SQL sem ter que projetar seus próprios métodos de criptografia.

- `Locação e renovação` : todos os segredos no Vault têm uma concessão associada a eles. No final da concessão, o Vault revogará automaticamente esse segredo. Os clientes podem renovar concessões por meio de APIs de renovação integradas.

- `Revogação` : o Vault possui suporte integrado para revogação secreta. O Vault pode revogar não apenas segredos individuais, mas uma árvore de segredos, por exemplo, todos os segredos lidos por um usuário específico ou todos os segredos de um tipo específico. A revogação auxilia na rolagem de chaves, bem como no bloqueio de sistemas no caso de uma intrusão.

## Architetura

![architecutre](./pics/architecture.png)

O Vault funciona principalmente com tokens e este é associado à uma política do cliente.

- Um token pode ser criado manualmente e atribuído aos clientes.
- Um cliente pode fazer login e obter um token.
  - A validacao de um cliente pode ser feita com diferentes integracoes como por exemplo:
    - kubernetes rbac
    - aws
    - azure
    - gcp

![flux](./pics/flux.png)

Observe que possui 4 estágios:

- 1 - `Autenticacao`. É o processo que o cliente fornece informacão ao vault para determinar se ele é quem diz ser. Somente depois de autenticado um token é gerado e acessociado a uma política.
- 2 - `Validacão` das credenciais passadas pelo usuário para vault. Pode ser validado de diferentes formas, usando LDAP, AppRole, Github, etc
- 3 - `Autorizacão` é o processo de verificacão das permissões de um cliente. O vault possui varia políticas de acesso aos endpoints que fornecem os segredos.
- 4 - `Acesso`. É quando o vault cria um token com base nas políticas associadas a identifidade do cliente. Com este token o cliente consegue recuperar os segredos.
  - Também é possível gerar um token fixo para acesso as chaves sem precisar passar por toda validacão.

>Existe o Vault SaaS da própria hashicorp ([HCP Vault](https://developer.hashicorp.com/hcp/docs/vault)), mas não é o objeto desse estudo.

## Como seguir esse estudo?

A primeira parte é um conhecimento básico para entender as funcionalidades do vault e alguns conceitos básicos

- [1 - Conceitos](./manuscritos/1%20-%20Conceitos.md)
- [2 - Instalacao CLI](./manuscritos/2%20-%20Instalacao%20CLI.md)
- [3 - Instalacao Server Inicial](./manuscritos/2%20-%20Instalacao%20CLI.md)
- [4 - Comandos Basicos](./manuscritos/4%20-%20Comandos%20Basicos.md)
- [5 - Basic UI](./manuscritos/5%20-%20UI.md)
- [6 - Secret Engine](./manuscritos/6%20-%20Secret%20Engine.md)
- [7 - Comandos Basicos](./manuscritos/7%20-%20Metodos%20de%20Autenticacao.md)
- [8 - Comandos Basicos](./manuscritos/8%20-%20Polices.md)

A segunda parte é criar um server vault configurado corretamente

- [9 - Instalação Avançada](./manuscritos/9%20-%20Instalando%20Avan%C3%A7ada.md)
- [10 - Conceitos Extras](./manuscritos/10%20-%20Conceitos%20Extras.md)
- [11 - Instalação Cluster HA](./manuscritos/11%20-%20Instalando%20Avan%C3%A7ada%20Cluster.md)
- [12 - Passoword Dinamico com AWS](./manuscritos/12%20-%20Dynamic%20Secrets%20and%20AWS%20Secrets%20Engine.md)
- [13 - SSH com OTP](./manuscritos/13%20-%20OTP.md)
- [14 - Instalando no Kubernetes](./manuscritos//14%20-%20Instalando%20no%20Kubernetes.md)

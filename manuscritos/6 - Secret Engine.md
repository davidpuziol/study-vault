# Secrets Engine

- São componentes que armazenam, geram ou criptografam dados.
- Os mecanismos de segredos são incrivelmente flexíveis, por isso é mais fácil pensar neles em termos de função. Os secrets engine recebem algum conjunto de dados, eles executam alguma ação nesses dados e retornam um resultado.
  - Existem vários secrets engine diferentes e não somente um que faz tudo.

Alguns secrets engine simplesmente armazenam e leem dados - como Redis/Memcached criptografados. Outros secrets engine se conectam a outros serviços e geram credenciais dinâmicas sob demanda. Outros secrets engine fornecem criptografia como serviço, geração totp, certificados e muito mais.

Os secrets engine são ativados em um caminho no Vault. Quando uma solicitação chega ao Vault, o roteador encaminha automaticamente qualquer coisa com o prefixo de rota para o mecanismo de segredos. Dessa forma, cada secret engine define seus próprios caminhos e propriedades.

Para o usuário, os secrets engine se comportam de maneira semelhante a um sistema de arquivos virtual, suportando operações como leitura, gravação e exclusão.

>Dependendo do secret engine que você esta utilizando ele possui outras funcionalidades no sistema. Por exemplo um tipo que gera password dinamicos, outro que conversa e entende como trabalhar com o iam da aws

Para listar rapidamente os nossos secrets engine que já estão habilitados por default vamos executar
> Vale lembrar que o secret/ só veio habilitado por que estamos ainda em -dev

```bash
# Observe nesser momento o type
❯ vault secrets list
Path          Type         Accessor              Description
----          ----         --------              -----------
cubbyhole/    cubbyhole    cubbyhole_7a017da3    per-token private secret storage
identity/     identity     identity_e76888ac     identity store
secret/       kv           kv_cf90e1db           key/value secret storage
sys/          system       system_129cb239       system endpoints used for control, policy and debugging
```

- sys/ endpoint do sistema usado para controle, polices e debugging, ainda não vamos utilizar
- identity/ vai gerenciar toda a parte de identity do nosso vault
  - Só esta enxegando porque estamos como root
- cubbyhole/ (cubilculo) é baseado por token.
  - O vault não acessa por usuário, somente por token que é recebido depois de passar pela authenticacão do usuário ou um token pré gerado estaticamente.
  - cada token ("usuário") possui o seu cubbyhoe que ele pode criar secrets arbitrárias ali dentro, é como se fosse a home de cada token. Um token não consegue pegar valores de outro. É o private secret storage de cada token.
- secret/ nada mais é do que um tipo do key value

Um pouco mais da documentacão podemos ver outros tipos de secret engine <https://developer.hashicorp.com/vault/docs/secrets>.

Cada secret engine possue suas funcionalidades. Na verdade é uma integracão para saber como trabalha com os secrets de terminada tecnologia.

## Ativando alguns secrets engines

- os secrets engines são montados como path no sistema.
- É da funcão do admin habilitar esses recursos e o ponto de montagem voce escolhe, não necessariamente sendo obrigado a utilizar o default.
- Pode ser habilitado vários paths com o mesmo tipo.

- Link para configuracao da aws
<https://developer.hashicorp.com/vault/docs/secrets/aws>

```bash

❯ vault secrets enable aws     
Success! Enabled the aws secrets engine at: aws/

❯ vault secrets list               
Path          Type         Accessor              Description
----          ----         --------              -----------
aws/          aws          aws_e8c5644a          n/a
cubbyhole/    cubbyhole    cubbyhole_7a017da3    per-token private secret storage
identity/     identity     identity_e76888ac     identity store
secret/       kv           kv_cf90e1db           key/value secret storage
sys/          system       system_129cb239       system endpoints used for control, policy and debugging

#criando dois com o mesmo tipo
# esse primeiro ele cria o path padrao com o mesmo nome da secret engine
❯ vault secrets enable ssh
Success! Enabled the ssh secrets engine at: ssh/

# neste segundo vamos passar um path personalizado
❯ vault secrets enable -path=myssh ssh
Success! Enabled the ssh secrets engine at: myssh/

# criando outro com o secret engine kv que ja tinhamos anteriormente no secret/
❯ vault secrets enable -path=mykv kv
Success! Enabled the kv secrets engine at: mykv/

# podemos observar os paths e os typos e constatar o que foi dito acima.
❯ vault secrets list
Path          Type         Accessor              Description
----          ----         --------              -----------
aws/          aws          aws_e8c5644a          n/a
cubbyhole/    cubbyhole    cubbyhole_7a017da3    per-token private secret storage
identity/     identity     identity_e76888ac     identity store
myssh/        ssh          ssh_94c7b6b3          n/a
secret/       kv           kv_cf90e1db           key/value secret storage
ssh/          ssh          ssh_443037b9          n/a
sys/          system       system_129cb239       system endpoints used for control, policy and debugging

~/Desktop/personal/study-vault main !2 ?2                                                                                                   1.3.3 14:32:39
❯ vault secrets enable -path=mykv kv
Success! Enabled the kv secrets engine at: mykv/

~/Desktop/personal/study-vault main !2 ?2                                                                                                   1.3.3 14:46:20
❯ vault secrets list                                                                                                              
Path          Type         Accessor              Description
----          ----         --------              -----------
aws/          aws          aws_e8c5644a          n/a
cubbyhole/    cubbyhole    cubbyhole_7a017da3    per-token private secret storage
identity/     identity     identity_e76888ac     identity store
mykv/         kv           kv_b9d5e01b           n/a
myssh/        ssh          ssh_94c7b6b3          n/a
secret/       kv           kv_cf90e1db           key/value secret storage
ssh/          ssh          ssh_443037b9          n/a
sys/          system       system_129cb239       system endpoints used for control, policy and debugging
```

Do mesmo modo poderíamos habilitar secret engines pela interface gráfica em `secrets>Add new engine` selecionar o tipo e configurar. Não existem todas as secrets engine na gui. Confira na documentação as disponíveis.

![newenginegui](../pics/uinewengine.png)

## Removendo e renomeando

Remover um path faz com que todas as secrets ali guardada sejam perdidas. Se precisar renomear nao precisa deletar, somente mover. Desta forma não é perdido.

```bash
# Desativando esse path perderiamos os dados
❯ vault secrets disable myssh/
Success! Disabled the secrets engine (if it existed) at: myssh/

# Renomeando esse path sem perder os dados.
❯ vault secrets move ssh/ my_new_ssh/
Started moving secrets engine ssh/ to my_new_ssh/, with migration ID 6b1cb26a-ae35-7e5a-4a91-dc90d1c07971
Success! Finished moving secrets engine ssh/ to my_new_ssh/, with migration ID 6b1cb26a-ae35-7e5a-4a91-dc90d1c07971

# conferindo
❯ vault secrets list                 
Path           Type         Accessor              Description
----           ----         --------              -----------
aws/           aws          aws_e8c5644a          n/a
cubbyhole/     cubbyhole    cubbyhole_7a017da3    per-token private secret storage
identity/      identity     identity_e76888ac     identity store
# veja que não passamos nenhuma descricao
my_new_ssh/    ssh          ssh_443037b9          n/a
mykv/          kv           kv_b9d5e01b           n/a
secret/        kv           kv_cf90e1db           key/value secret storage
sys/           system       system_129cb239       system endpoints used for control, policy and debugging
```

Poderíamos ter passado uma descricão, mas esquecemos. Para editar vamos usar o tune para editar.

>O tune faz muito mais coisas do que somente colocar novamente a descricão e veremos sobre ele mais pra frente.

```bash
❯ vault secrets tune -description="Minhas sshs" my_new_ssh/
Success! Tuned the secrets engine at: my_new_ssh/

❯ vault secrets tune -description="Iam aws" aws/       
Success! Tuned the secrets engine at: aws/

❯ vault secrets list                                               
Path           Type         Accessor              Description
----           ----         --------              -----------
aws/           aws          aws_e8c5644a          Iam aws
cubbyhole/     cubbyhole    cubbyhole_7a017da3    per-token private secret storage
identity/      identity     identity_e76888ac     identity store
my_new_ssh/    ssh          ssh_443037b9          Minhas sshs
mykv/          kv           kv_b9d5e01b           n/a
secret/        kv           kv_cf90e1db           key/value secret storage
sys/           system       system_129cb239       system endpoints used for control, policy and debugging

```

Para saber um pouco mais sobre algum path utilize o comando `vault path-help <nome do path sem o />`

faz uns testes:

- vault path-help my_new_ssh
- vault path-help sys
- vault path-help aws
- vault path-help cubbyhole

---

## Dividindo secrets

Não necessariamente somente criar uma secret direto na raiz do path. Podemos utilizar o path para gerar subgrupos também.

```bash
❯ vault kv put mykv/pessoais/david token=ahuidhuada user=admin password=123change
Success! Data written to: mykv/pessoais/david


❯ vault kv put mykv/pessoais/outro token=hudfahuda user=guest password=123
Success! Data written to: mykv/pessoais/outro

#Por default ele quando criamos não passamos a versão desse secret engine do tipo kv. Logo ele pegou a default
❯ vault kv metadata get mykv/pessoais/david
Metadata not supported on KV Version 1

❯ vault kv enable-versioning mykv          
Success! Tuned the secrets engine at: mykv/

# Agora sim.
❯ vault kv metadata get mykv/pessoais/david 
======= Metadata Path =======
mykv/metadata/pessoais/david

========== Metadata ==========
Key                     Value
---                     -----
cas_required            false
created_time            2023-01-04T01:42:28.420082188Z
current_version         1
custom_metadata         <nil>
delete_version_after    0s
max_versions            0
oldest_version          1
updated_time            2023-01-04T01:42:28.420082188Z

====== Version 1 ======
Key              Value
---              -----
created_time     2023-01-04T01:42:28.420082188Z
deletion_time    n/a
destroyed        false
```

 Confira na Gui que verá a divisão de pasta

 ![kvgroup](../pics/kvgroup.png)

 O que acontece se ele tentar deletar o mykv/pessoais? Irá deletar os david e outro?

```bash
 ❯ vault kv delete mykv/pessoais
Success! Data deleted (if it existed) at: mykv/data/pessoais

❯ vault kv list mykv/pessoais
Keys
----
david
outro

# Não deletou
mykv/data/pessoais/david

======= Metadata =======
Key                Value
---                -----
created_time       2023-01-04T01:42:28.420082188Z
custom_metadata    <nil>
deletion_time      n/a
destroyed          false
version            1

====== Data ======
Key         Value
---         -----
password    123change
token       ahuidhuada
user        admin
```

Agora olha que interessante, podemos criar um kv para mykv/pessoais

```bash
❯ vault kv put mykv/pessoais local=home work=homeoffice                   
=== Secret Path ===
mykv/data/pessoais

======= Metadata =======
Key                Value
---                -----
created_time       2023-01-04T01:53:43.010929184Z
custom_metadata    <nil>
deletion_time      n/a
destroyed          false
version            1

❯ vault kv list mykv/       
Keys
----
pessoais
pessoais/

Keys
----
david
outro
```

 ![kvgroup2](../pics/kvgroup2.png)

 >Resumindo uma chave tem um path único. Quando usamos chaves que possuem o mesmo patch ele facilitam a listagem, mas não é possível deletar de uma vez aquele "grupo". O mesmo se reflete na interface gráfica, confira.

Dicas:

**`DICA VALIOSA: SOMENTE COLOQUE SECRETS NO ULTIMO NÍVEL, FICARÁ MAIS FACIL PARA APLICAR AS POLICIES DEPOIS. POR EXEMPLO TER secret/pessoal e secret/pessoal/david não é boa idéia. o ideial serial secret/pessoal/david e outra secret/personal OU QUALQUER COISA TIPO ISSO.`**

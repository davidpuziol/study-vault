# OTP e SSH Secret Engine

OTP é o one time password é uma secret que irá valer por um período curto de tempo.

Para que funciona adequadamente dentro da máquina exemplo vamos instalar o vault ssh helper que fará o trabalho de gerar um ssh key para nós.

```bash
❯ vault secrets list
Path          Type         Accessor              Description
----          ----         --------              -----------
aws/          aws          aws_bcc399b1          AWS IAM
cubbyhole/    cubbyhole    cubbyhole_7c7a7f10    per-token private secret storage
identity/     identity     identity_0dd9718b     identity store
ssh/          ssh          ssh_5a2482fb          n/a
sys/          system       system_d63da29b       system endpoints used for control, policy and debugging
```

Vamos usar um uma máquina do próprio vault como exemplo. Entre via ssh na máquina do vault que vc deseja instalar esse vault helper

No [github da hashicorp](https://github.com/hashicorp) podemos encontrar várias coisas que auxiliam o vault. Faça um filtro por vault nos repositórios e verá quanta coisa boa <https://github.com/orgs/hashicorp/repositories?language=&q=vault&sort=&type=all>

Vamos usar aqui o [vault ssh helper](https://github.com/hashicorp/vault-ssh-helper)
<https://developer.hashicorp.com/vault/tutorials/secrets-management/ssh-otp>

```bash
sudo su -
sudo apt-get install unzip -y
wget https://releases.hashicorp.com/vault-ssh-helper/0.2.1/vault-ssh-helper_0.2.1_linux_amd64.zip
unzip vault-ssh-helper_0.2.1_linux_amd64.zip 
mv vault-ssh-helper /usr/local/bin/
mkdir /etc/vault-ssh-helper.d

# Como esta na mesma rede do vault, podemos passar o ip interno
cat > /etc/vault-ssh-helper.d/config.hcl <<EOF
vault_addr = "http://10.10.10.12:8200"
tls_skip_verify = false
ssh_mount_point = "ssh"
allowed_roles = "*"
allowed_cidr_list = "0.0.0.0/0"
EOF

#Vamos editar o arquivo abaixo, mas antes gerar uma cópia
cp /etc/pam.d/sshd /etc/pam.d/sshd.default
# comentar
sed -e '/@include common-auth/s/^/#/g' -i file
# incluir
echo "auth requisite pam_exec.so quiet expose_authtok log=/tmp/vaultssh.log /usr/local/bin/vault-ssh-helper -config=/etc/vault-ssh-helper.d/config.hcl" >> /etc/pam.d/sshd
echo "auth optional pam_unix.so not_set_pass use_first_pass nodelay" >> /etc/pam.d/sshd

#editando o arquivo abaixo mas gerando um backup antes
cp /etc/ssh/sshd_config /etc/ssh/sshd_config_original

sed -i 's/KbdInteractiveAuthentication no/KbdInteractiveAuthentication yes/g' /etc/ssh/sshd_config
sed -i 's/UsePAM no/UsePAM yes/g' /etc/ssh/sshd_config 
sed -i 's/PasswordAuthentication yes/PasswordAuthentication no/g' /etc/ssh/sshd_config 

systemctl restart sshd

# Conferindo se a configuração esta ok
vault-ssh-helper -verify-only -dev -config /etc/vault-ssh-helper.d/config.hcl
```

voltando para onde esta configurando, na sua maquina local ou dentro da máquina local é preciso criar a role agora para poder gerar essa key temporária para esta maquina.

```bash
vault write ssh/roles/otp_key_vault3 key_type=otp default_user=ubuntu cidr_list=0.0.0.0/0
```

Agora vamos criar uma policy com permissão para essa role e vincular a algum usuário que possa fazer login no vault. Essa role estará disponivel em ssh/creds/otp_key_vault3.

o arquivo usado será com esta policie

```json
path "ssh/creds/otp_key_vault3" {
    capabilities = ["create","read","update"]
}
```

```bash
❯ vault policy write vault3 ./resources/vault3-policy.hcl
Success! Uploaded policy: vault3
```

vamos habilitar o userpass poder criar um usuário e definir um com esta police

```bash
❯ vault auth enable userpass
❯ vault auth list
❯ vault write auth/userpass/users/david password="abc123mudar" policies="vault3"
```

Agora faca o login com este usuário e vamos gerar o nosso one time password para fazer login ma máquina

```bash
❯ vault login -method=userpass username=david password=abc123mudar
WARNING! The VAULT_TOKEN environment variable is set! The value of this
variable will take precedence; if this is unwanted please unset VAULT_TOKEN or
update its value accordingly.

Success! You are now authenticated. The token information displayed below
is already stored in the token helper. You do NOT need to run "vault login"
again. Future Vault requests will automatically use this token.

Key                    Value
---                    -----
token                  hvs.CAESILxGm1JwJ_v-BeHPS6JA03vlUYMhpyn2d-hF4meg8UfPGh4KHGh2cy5mdE1nRnFZOWNUWWdicE9VcTh5OFNBcXQ
token_accessor         D8nOi4zw7flyY7z80TaTCgvy
token_duration         6h
token_renewable        true
token_policies         ["default" "vault3"]
identity_policies      []
policies               ["default" "vault3"]
token_meta_username    david
```

agora conectando usando esse passworkd

```bash
❯ sshpass -p 2044cc8e-f7df-54db-2d84-ea80fe932d043 ssh ubuntu@34.213.208.189
Welcome to Ubuntu 22.04.1 LTS (GNU/Linux 5.15.0-1026-aws x86_64)

 * Documentation:  https://help.ubuntu.com
 * Management:     https://landscape.canonical.com
 * Support:        https://ubuntu.com/advantage

  System information as of Sat Jan  7 03:03:35 UTC 2023

  System load:  0.060546875       Processes:             108
  Usage of /:   33.8% of 7.57GB   Users logged in:       1
  Memory usage: 30%               IPv4 address for eth0: 10.10.10.5
  Swap usage:   0%

 * Ubuntu Pro delivers the most comprehensive open source security and
   compliance features.

   https://ubuntu.com/aws/pro

11 updates can be applied immediately.
To see these additional updates run: apt list --upgradable


Last login: Sat Jan  7 03:02:05 2023 from 164.163.205.170
ubuntu@vault-03:~$ 

```

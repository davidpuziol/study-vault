# Policies

<https://developer.hashicorp.com/vault/docs/concepts/policies>

As pólices regras utilizadas para permitir acesso (e ações) a uma determinada path.

Por default as policies são para bloquear (Deny) e vamos liberando de acordo com a necessidade.

Em tese especifica-se uma path e quais as ações sobre ela são liberadas. Com uma policy definida vinculamos ela para os usuários que terão este privilégio.

>As ações são praticamente os subcomandos da cli, logo se tiver habilitado o auto-complete ficará mais fácil ver se precisar ir a documentação.

Por padrão duas policies já existem:

- `root`, sem nada dentro liberando tudo
- `default` que todos que não tem uma policy já recebem esta.

![policesdefault](../pics/policy.png)
![policedefault](../pics/policyshcl.png)

Usa-se a linguagem HCL da hashicorp para escrever as policies.

Se analisar os verbs na policy default veremos alguns verbos:

- update
- read
- create
- delete
- list

Ainda temos outros que vamos ver depois:

- sudo
- deny

veja esta policy do cubbyhole e já que tudo nela é habilitado.

```bash
# Allow a token to manage its own cubbyhole
# Observe que qualquer path após o cubbyhole/ receberá esta policy
path "cubbyhole/*" {
    capabilities = ["create", "read", "update", "delete", "list"]
}
```

Agora começa a fazer também mais sentido colocar secrets agrupadas.

## Criando Police pela UI

>Utiliza navegador diferente se quiser manter dois tokens diferentes para dois acesso ao vault simultanemanente, um com root e outro com usuário qualquer. Se usar somente navegador os tokens se confudirão na sessão.

Como root, vá em policies > Create ACL policy

E usando mesmo padrão acima vamos fazer

![mykv_acl](../pics/mykv_alc.png)

Observe que existe uma herança incluida ali dentro.

- A primeira regra diz que todo mundo após mykv/ pode ser listado
- A segunda diz que mykv/pessoais pode ser lido, logo, incluirá os paths que temos mykv/pessoais/david e mykv/pessoais/outros
- A terceira regra diz que somente os paths dentro de mykv/pessoais/ que é david e outros podem sofrer update

Agora vamos criar um usuário já passando a policy criada anteriormente.

```bash

❯ vault policy list
default
mykv_acl
root

❯ vault write auth/userpass/users/david password=abc123mudar policies=mykv_acl
Success! Data written to: auth/userpass/users/david
```

Vamos fazer o login do usuário novo pela pela gui, nesse caso o `david`, em outro navegador, e observar que apareceu agora a nossa mykv/ ali. Conseguimos listar, mas não temos permissão para ver as secrets. Isso acontece pois temos um data escondido logo depois do mykv/ por isso a primeira regra que continha o `*` também pegou `data`.

Voltando ao navegador com root, edite a regra mykv_acl como esta abaixo incluindo o data e salve.

```bash
path "mykv/*" {
    capabilities = ["list"]
}

path "mykv/data/pessoais" {
    capabilities = ["read"]
}

path "mykv/data/pessoais/*" {
    capabilities = ["read","update"]
}
```

No navegador com o usuário david e tente acessar e veja que agora tudo funciona

Delete a policy na sessão com o root e vamos reescrever ela pelo cli do vault. Nesse momento o usuário david já irá perder a permissão e não terá mais essa policy.

> Deletar uma policy já desassocia esta policy a todos os usuários vinculados a ela. Atenção que faremos mais testes a frente.

Para conhecimento,delete também os usuários pela gui e veja todos os que tem as policies vinculadas a ele. Obviamente na conta root para fazer isso.

Para fazer isso faça um tour na aba  `Access > Entities`. Veremos também que temos outros recursos lá como, groups, oidc, leases, multifactor, etc.

## Criando a policy pelo cli

A primeira coisa a se fazer é criar um arquivo com a police escrita.
> É boa pratica nomear os arquivo com .hcl no final para identificação, apesar de que para o linux não existe diferença.

vamos aprender além do uso do `*` o uso do `+`.

- o sinal de `+` é utilizando para determinar um nivel
- o sinal de `*` é usado para substituir qualquer coisa.

Crie um arquivo my-polices.hcl com o conteúdo abaixo, mas removendo o comentários. O arquivo criado que será usado está em [my-police.hcl](../resources/my-policy.hcl).

```bash
# todos daqui pra frente receberão o list
path "mykv/*" {
    capabilities = ["list"]
}           
#todos desse nível receberão o read 
path "mykv/data/pessoal" {                  
    capabilities = ["read"]
}                 

# Não existe a necessidade de aplicar o list, apesar de poder, pois foi aplicado na primeira regra.
# Porém existe a necessidade de aplicar o read pois a segunda regra não aplicou para tudo, somente para o nível
# tudo que estiver no terceiro nivel irá receber read e update
# Não seria possivel aplicar mykv/data/*/* pois o primeira * já faria referencia a tudo. Quando se fala tudo, é dali pra frente.   
path "mykv/data/+/*" {
    capabilities = ["read","update", "create", "delete"]
}
```

vamos agora escrever a policy e criar um user com ela.

```bash
❯ vault policy write mykv_acl ./resources/my-policy.hcl
Success! Uploaded policy: mykv_acl

# Criando uma outra para deletar só a nivel de curiosidade
❯ vault policy write mykv_acl_to_delete ./resources/my-policy.hcl
Success! Uploaded policy: mykv_acl_to_delete

❯ vault policy delete mykv_acl_to_delete
Success! Deleted policy: mykv_acl_to_delete

❯ vault policy list                     
default
mykv_acl
root

# Vamos criar o mesmo user, pois deletamos anteriormente, para reaproveitar o comando.
❯ vault write auth/userpass/users/david password=abc123mudar policies=mykv_acl
Success! Data written to: auth/userpass/users/david

# Vamos aproveitar e criar um usuario david_teste com uma policy que não existe
❯ vault write auth/userpass/users/david_teste password=abc123mudar policies=blablabla
Success! Data written to: auth/userpass/users/david_teste
```

faça um login com o usuário david_teste em um navegador e em outro root verifique as entidades e a police dela.
Verá que não existe police associada a este usuário.

Agora vamos subir uma police com o nome blablabla depois que um usuário já foi criado com ela. O que acontece?
>Funciona. Observe que se um usuário estiver com uma police vinculada e ela não existir, o vault irá tentar buscar a police e não irá conseguir, mas irá buscar. Se por um evento um usuário tiver police vinculada e ela for criada depois ele assumirá.

```bash
❯ vault policy delete blablabla             
Success! Deleted policy: blablabla

❯ vault read auth/userpass/users/david_teste
Key                        Value
---                        -----
policies                   [blablabla]
token_bound_cidrs          []
token_explicit_max_ttl     0s
token_max_ttl              0s
token_no_default_policy    false
token_num_uses             0
token_period               0s
token_policies             [blablabla]
token_ttl                  0s
token_type                 default
```

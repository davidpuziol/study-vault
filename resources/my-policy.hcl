path "mykv/*" {
    capabilities = ["list"]
}

path "mykv/data/pessoal" {
    capabilities = ["read"]
}                 
   
path "mykv/data/+/*" {
    capabilities = ["read","update", "create", "delete"]
}
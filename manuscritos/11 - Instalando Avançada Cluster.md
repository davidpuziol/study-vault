# Instalanção Avançada cluster

Essa instalação irá melhorar o que vimos na intação avançada.

Nessa instalação vamos agora criar:

- 3 vms para o vault
- 3 vms para o consul para poder passar o storage que era file para um serviço de alta disponibidade

Todas as vms foram colocaram em uma rede pública, foi criado um security group só para todo mundo.
Essas circunstancias são para facilitar no processo configuração, mas não deve ser feito em produção. O foco aqui é na instalação para produção e não na infra estrutura. O certo seria todo mundo na rede privada e um load balancer na frente.

## Requisitos

Crie 6 máquinas ubuntu t2 micro e coloque elas no mesmo security group com as seguintes regras:
Se vc quer aproveitar a máquina anterior, pode e somente crie mais 5.
![vms](../pics/vmsrequired.png)

![secgrouprules](../pics/secgrouprules.png)
Resumindo elas tem comunição entre si, com as portas 8200 8201 8500 e 22 liberadas.

## Instalação do cluster consul

Todo esse processo deve ser feito para cada um dos nodes consul

```bash
sudo su -
# mude de o nome de acordo com o node que vc esta
hostname consul-01
echo consul-01 > /etc/hostname 

#Criando os diretórios que precisaremos e o usuário de serviço do consul
mkdir /etc/consul
mkdir /var/lib/consul
mkdir /var/log/consul
touch /etc/consul/consul.json

useradd -r consul

# baixando e instalando o consul
wget -O- https://apt.releases.hashicorp.com/gpg | gpg --dearmor | sudo tee /usr/share/keyrings/hashicorp-archive-keyring.gpg
echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] https://apt.releases.hashicorp.com $(lsb_release -cs) main" | sudo tee /etc/apt/sources.list.d/hashicorp.list
sudo apt update && sudo apt install consul

consult --version

# configurando o consul.json. Nessa parte precisa saber o endereço privado das 3 vms
# 1 - faca alteracao no node_name para o node em questão
# 2 - o advertise_address precisa ser o ip privado da própria vm
# 3 - retry_join precisa conter a lista do ip privado das máquinas.

cat >> /etc/consul/consul.json <<EOF
{
   "server":true,
   "node_name":"consul-01",
   "datacenter":"dc1",
   "data_dir":"/var/lib/consul/data",
   "bind_addr":"0.0.0.0",
   "client_addr":"0.0.0.0",
   "advertise_addr":"10.10.10.4",
   "bootstrap_expect":3,
   "retry_join":[
      "10.10.10.4",
      "10.10.10.9",
      "10.10.10.13"
   ],
   "ui":true,
   "log_level":"DEBUG",
   "enable_syslog":true,
   "acl_enforce_version_8":false
}
EOF

# colocando as permissões nas pastas para o usuário

chown -R consul:consul /etc/consul
chown -R consul:consul /var/lib/consul
chown -R consul:consul /var/log/consul

cat >> /etc/systemd/system/consul.service <<EOF
[Unit]
Description=Consul server agent
Requires=network-online.target
After=network-online.target
ConditionFileNotEmpty=/etc/consul/consul.json

[Service]
User=consul
Group=consul
Restart=on-failure
PermissionsStartOnly=true
ExecStartPre=-/bin/mkdir -p /var/log/consul
ExecStartPre=-/bin/mkdir -p /var/lib/consul
ExecStartPre=/bin/chown -R consul:consul /var/lib/consul
ExecStartPre=/bin/chown -R consul:consul /var/log/consul
ExecStartPre=/bin/chown -R consul:consul /etc/consul    
ExecStart=/usr/bin/consul agent -config-file=/etc/consul/consul.json
ExecReload=/bin/kill -HUP $MAINPID
KillMode=process
KillSignal=SIGTERM
RestartSec=42s
pid_file="/var/lib/consul/consul.pid"
StandardOutput=append:/var/log/consul/output.log
StandardError=append:/var/log/consul/error.log

[Install]
WantedBy=multi-user.target
EOF

systemctl daemon-reload
systemctl enable consul --now
systemctl status consul | grep Active

#A linha abaixa é somente para testar
consul operator raft list-peers
Node       ID                                    Address           State     Voter  RaftProtocol
consul-03  1a5b295d-9746-7d51-6106-8f8c93867e3d  10.10.10.13:8300  leader    true   3
consul-01  d458d5f6-f0e3-b783-5234-e21a5e317090  10.10.10.4:8300   follower  true   3
consul-02  cf81faf8-59d2-fa23-63ad-6c85b48ea4ff  10.10.10.9:8300   follower  true   3
```

![consulcluster](../pics/clusterconsul.png)

>O consult além de ser um storage backend para guadar nossas chaves também funciona como service mesh.

## Instalando o cluster vault

Do mesmo modo todo o processo aqui deve ser feito nas 3 vms

```bash
sudo su -

hostname vault-01
echo $(hostname) > /etc/hostname

# Faca um update dos repos para 
sudo apt update && sudo apt install gpg lsb_release -y
wget -O- https://apt.releases.hashicorp.com/gpg | gpg --dearmor | sudo tee /usr/share/keyrings/hashicorp-archive-keyring.gpg >/dev/null
gpg --no-default-keyring --keyring /usr/share/keyrings/hashicorp-archive-keyring.gpg --fingerprint
echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] https://apt.releases.hashicorp.com $(lsb_release -cs) main" | sudo tee /etc/apt/sources.list.d/hashicorp.list
sudo apt update && sudo apt install vault
vault -autocomplete-install
bash

# A explicação para a estrutura de instalação já foi feita anteriormente
useradd --system vault
mkdir /var/log/vault
mkdir /var/lib/vault
mkdir /var/lib/vault/data
ln -s /var/log/vault/ /var/lib/vault/logs
mkdir /etc/vault
touch /etc/vault/config.hcl

chown -R vault:vault /var/lib/vault
chown -R vault:vault /var/log/vault
chown -R vault:vault /etc/vault

# observe que ele aponta para a propria instancia do vault no storage backend, porém teremos um consul client instalado também
# o endereco aqui 10.10.10.12 é da prórpia vm. Mude cada um desses endereços para o endreço da sua instância.
# api_addr podia receber o ip de um load balancer que levasse para as máquinas que estão atrás.

cat >> /etc/vault/config.hcl <<EOF
storage "consul" {
  address = "127.0.0.1:8500"
  path = "vault/"
}

listener "tcp" {
  address     = "0.0.0.0:8200"
  cluster_address = "10.10.10.12:8201"
  tls_disable = 1
}

api_addr = "http://0.0.0.0:8200"
cluster_addr = "http://10.10.10.12:8201"
ui = true
cluster_name = "vault"
disable_mlock = true
max_lease_ttl = "12h"
default_lease_ttl = "6h"
pid_file = "/var/lib/vault/vault.pid"
EOF

# Agora vamos criar o service

cat > /etc/systemd/system/vault.service <<EOF
[Unit]
Description = Vault Server
Requires = network-online.target
After = network-online.target
ConditionFileNotEmpty = /etc/vault/config.hcl

[Service]
User = vault
Group = vault
Restart = on-failure
ExecStartPre=-/bin/mkdir -p /var/log/vault
ExecStartPre=-/bin/mkdir -p /var/lib/vault/data
ExecStartPre=/bin/chown -R vault:vault /var/lib/vault
ExecStartPre=/bin/chown -R vault:vault /var/log/vault
ExecStartPre=/bin/chown -R vault:vault /etc/vault    
ExecStart = /usr/bin/vault server -config=/etc/vault/config.hcl
StandardOutput = append:/var/log/vault/output.log
StandardError = append:/var/log/vault/error.log
ExecReload = /bin/kill -HUP 
KillMode=process
KillSignal=SIGTERM
LimitMEMLOCK = infinity
[Install]

WantedBy = multi-user.target
EOF

echo "export VAULT_ADDR='http://127.0.0.1:8200'" >> /root/.bashrc

systemctl daemon-reload 

# Ainda não faça start, pois precisamos configurar o consult cliente
```

Agora instalando o consul client. Esse cliente irá fazer a comunicação com o cluster consul. Por isso que na hora de definir o storage colocamos um endereço local. Isso é uma boa prática.

```bash
#Criando os diretórios que precisaremos e o usuário de serviço do consul
mkdir /etc/consul
mkdir /var/lib/consul
mkdir /var/log/consul
touch /etc/consul/consul.json

useradd -r consul

# baixando e instalando o consul
wget -O- https://apt.releases.hashicorp.com/gpg | gpg --dearmor | sudo tee /usr/share/keyrings/hashicorp-archive-keyring.gpg
echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] https://apt.releases.hashicorp.com $(lsb_release -cs) main" | sudo tee /etc/apt/sources.list.d/hashicorp.list
sudo apt update && sudo apt install consul

consult --version

# configurando o consul.json client. Nessa parte precisa saber o endereço privado das 3 vms
# 1 - faca alteracao no node_name para o node em questão nesse caso é o nome da maquina do vault
# 2 - o advertise_address precisa ser o ip privado da própria vm
# 3 - retry_join precisa conter a lista do ip privado das máquinas do consul

cat >> /etc/consul/consul.json <<EOF
{
   "server":false,
   "node_name":"vault-01",
   "datacenter":"dc1",
   "data_dir":"/var/lib/consul/data",
   "bind_addr":"10.10.10.12",
   "client_addr":"127.0.0.1",
   "retry_join":[
      "10.10.10.4",
      "10.10.10.9",
      "10.10.10.13"
   ],
   "log_level":"DEBUG",
   "enable_syslog":true,
   "acl_enforce_version_8":false
}
EOF

cat >> /etc/systemd/system/consul.service <<EOF
[Unit]
Description=Consul server agent
Requires=network-online.target
After=network-online.target
ConditionFileNotEmpty=/etc/consul/consul.json

[Service]
User=consul
Group=consul
Restart=on-failure
PermissionsStartOnly=true
ExecStartPre=-/bin/mkdir -p /var/log/consul
ExecStartPre=-/bin/mkdir -p /var/lib/consul/data
ExecStartPre=/bin/chown -R consul:consul /var/lib/consul
ExecStartPre=/bin/chown -R consul:consul /var/log/consul
ExecStartPre=/bin/chown -R consul:consul /etc/consul    
ExecStart=/usr/bin/consul agent -config-file=/etc/consul/consul.json
ExecReload=/bin/kill -HUP $MAINPID
KillMode=process
KillSignal=SIGTERM
RestartSec=42s
pid_file="/var/lib/consul/consul.pid"
StandardOutput=append:/var/log/consul/output.log
StandardError=append:/var/log/consul/error.log

[Install]
WantedBy=multi-user.target
EOF

systemctl daemon-reload
```

Agora vamos iniciar o nossos serviços e já ativando para um futuro reboot

```bash
systemctl enable consul --now
systemctl enable vault --now
```

Depois de tudo pronto o consul tem que identificar todos os server e clientes.
![consulnodes](../pics/consulnodes.png)

## Configuranndo o vault

Em alguma da máquinas do vault...
lembrando de ter export feito para o server

 vamos verificar o status e reparar que agora o HA está Enabled (significa que temos um cluster), o storage é o consult e precisamos iniciar o nosso cluster do vault.

```bash
export VAULT_ADDR='http://127.0.0.1:8200'

vault status
# Key                Value
# ---                -----
# Seal Type          shamir
# Initialized        false
# Sealed             true
# Total Shares       0
# Threshold          0
# Unseal Progress    0/0
# Unseal Nonce       n/a
# Version            1.12.2
# Build Date         2022-11-23T12:53:46Z
# Storage Type       consul
# HA Enabled         true
```

Para iniciar termos todos aqueles passos que devemos seguir.

## Inicializando o Vault

Observe que no serviço já colocamos para levantar a variavel de ambiente VAULT_ADD para ele mesmo.
Agora precisamo inicializar o server para gerar os tokens e as chaves para unseal.

```bash
vault status

# Key                Value
# ---                -----
# Seal Type          shamir
# # Observe que ele não veio inicializado e veio sealed, diferente do modo -dev que nos traz essa facilidade
# Initialized        false
# Sealed             true
# Total Shares       0
# Threshold          0
# Unseal Progress    0/0
# Unseal Nonce       n/a
# Version            1.12.2
# Build Date         2022-11-23T12:53:46Z
# Storage Type       file
# HA Enabled         false

vault operator init -key-shares=3-key-threshold=2 | tee /var/lib/vault/initvault.txt

vault status # Só para conferir

# Vamos separar os arquivos das chaves para guardar depois.
cat /var/lib/vault/initvault.txt | grep "Root Token" | awk '{print $4}' | tee /var/lib/vault/root.token

cat /var/lib/vault/initvault.txt | grep "Unseal Key" | awk '{print $4}' | tee /var/lib/vault/unsealed.keys

# Fazendo o unseal do vault
vault operator unseal $(cat /var/lib/vault/unsealed.keys | awk 'NR ==1')
vault operator unseal $(cat /var/lib/vault/unsealed.keys | awk 'NR ==2')

# Repare agora que temos o HA mod active, o nome do cluster, o cluster id, ha habilitado e o endereço da interface de comunicação dessa instancia que é o HA cluster.
root@vault-01:~# vault status
# Key             Value
# ---             -----
# Seal Type       shamir
# Initialized     true
# Sealed          false
# Total Shares    3
# Threshold       2
# Version         1.12.2
# Build Date      2022-11-23T12:53:46Z
# Storage Type    consul
# Cluster Name    vault
# Cluster ID      d37cac15-884d-bb90-6071-7664998ee4b1
# HA Enabled      true
# HA Cluster      https://10.10.10.12:8201
# HA Mode         active
# Active Since    2023-01-06T18:16:41.845254041Z

# A geração de um segundo token root fica ao seu critério

# Esse passo pode ser colocado em todos as instancias para que quando entre como root já esteja com o token no env, facilita a vida.
export VAULT_TOKEN=$(cat /var/lib/vault/root.token| awk 'NR ==1')
echo "export VAULT_TOKEN='$(cat /var/lib/vault/root.token| awk 'NR ==1')'" >> /root/.bashrc
```

Agora em outras máquinas vault precisamos fazer o unseal também. Execute esses passos nelas. Dessa vez vc precisará das senhas de unseal que pode conseguir no primeiro com o coamando `cat /var/lib/vault/unsealed.keys`

```bash
#substitua as chaves pelos valores
vault operator unseal chave1
vault operator unseal chave2

# Observe que este vault fica no stado de standby em HA Mode. Ele só entra em ação quando o primeiro cair.
vault status
# Key                    Value
# ---                    -----
# Seal Type              shamir
# Initialized            true
# Sealed                 false
# Total Shares           3
# Threshold              2
# Version                1.12.2
# Build Date             2022-11-23T12:53:46Z
# Storage Type           consul
# Cluster Name           vault
# Cluster ID             d37cac15-884d-bb90-6071-7664998ee4b1
# HA Enabled             true
# HA Cluster             https://10.10.10.12:8201
# HA Mode                standby
# Active Node Address    http://0.0.0.0:8200
```

Por ultimo ode fazer um checkup da instalação. Ainda temos problemas, lógico, que é por conta do tls onde apontarei na saída. Com relação aos warning.

- Como usamos t2.micro temos pouco espaço em disco.
- Não habilitamos telemetria.
- Nosso consul não tem tls
- Nosso vault não tem tls

```bash
root@vault-01:~# vault operator diagnose -config /etc/vault/config.hcl
Vault v1.12.2 (415e1fe3118eebd5df6cb60d13defdc01aa17b03), built 2022-11-23T12:53:46Z

Results:
[ failure ] Vault Diagnose: HCP link check will not run on OSS Vault.
  [ warning ] Check Operating System
    [ success ] Check Open File Limits: Open file limits are set to 1048576.
    [ success ] Check Disk Usage: / usage ok.
    [ warning ] Check Disk Usage: /snap/amazon-ssm-agent/6312 is 100.00 percent full.
      It is recommended to have more than five percent of the partition free.
    [ warning ] Check Disk Usage: /snap/core18/2632 is 100.00 percent full.
      It is recommended to have more than five percent of the partition free.
    [ warning ] Check Disk Usage: /snap/core20/1695 is 100.00 percent full.
      It is recommended to have more than five percent of the partition free.
    [ warning ] Check Disk Usage: /snap/lxd/23541 is 100.00 percent full.
      It is recommended to have more than five percent of the partition free.
    [ warning ] Check Disk Usage: /snap/snapd/17883 is 100.00 percent full.
      It is recommended to have more than five percent of the partition free.
    [ warning ] Check Disk Usage: /snap/core18/2667 is 100.00 percent full.
      It is recommended to have more than five percent of the partition free.
    [ warning ] Check Disk Usage: /snap/core20/1778 is 100.00 percent full.
      It is recommended to have more than five percent of the partition free.
  [ success ] Parse Configuration
  [ warning ] Check Telemetry: Telemetry is using default configuration
    By default only Prometheus and JSON metrics are available.  Ignore this warning if you are using telemetry or are using these metrics and are
    satisfied with the default retention time and gauge period.
  [ success ] Check Storage
    [ success ] Create Storage Backend
    [ skipped ] Check Consul TLS: HTTPS is not used, Skipping TLS verification.
    [ success ] Check Consul Direct Storage Access
    [ success ] Check Storage Access
  [ success ] Check Service Discovery
    [ skipped ] Check Consul Service Discovery TLS: HTTPS is not used, Skipping TLS verification.
    [ success ] Check Consul Direct Service Discovery
  [ success ] Create Vault Server Configuration Seals
  [ skipped ] Check Transit Seal TLS: No transit seal found in seal configuration.
  [ success ] Create Core Configuration
    [ success ] Initialize Randomness for Core
  [ success ] HA Storage
    [ success ] Create HA Storage Backend
    [ skipped ] Check HA Consul Direct Storage Access: No HA storage stanza is configured.
  [ success ] Determine Redirect Address
  [ success ] Check Cluster Address: Cluster address is logically valid and can be found.
  [ success ] Check Core Creation
  [ skipped ] Check For Autoloaded License: License check will not run on OSS Vault.
  [ failure ] Start Listeners
    [ warning ] Check Listener TLS: Listener at address 0.0.0.0:8200: TLS is disabled in a listener config stanza.
    [ failure ] Create Listeners: Error initializing listener of type tcp: listen tcp4 0.0.0.0:8200: bind: address already in use
  [ skipped ] Check Autounseal Encryption: Skipping barrier encryption test. Only supported for auto-unseal.
  [ success ] Check Server Before Runtime
  [ success ] Finalize Shamir Seal
```

Fazendo um checkup vemos o que podemos melhorar para atender todos os requisitos para uma instalação perfeita. Ainda não é o caso, pois estamos melhorando as coisas.

```bash
vault operator diagnose -config /etc/vault/config.hcl
```

teste a disponibilidade do cluster parando a machina 1 do vault e 1 do consul 1. Faça seus testes.

Se vc fez isso irá entender que ela volta selada novamente. Por isso precisamos fazer o auto unseal.

# Comandos Basicos

Utilizando a instalacão do server anterior com o export já feito do server na nossa máquina local, vamos ver alguns comandos.

Antes de comecar so para relembrar

```bash
export VAULT_ADDR='http://ec2-18-236-71-160.us-west-2.compute.amazonaws.com:8200'
```

>É necessaŕio liberar a porta 8200 também do no security group da ec2
![sg](../pics/secgroup.png)

A primeira coisa que precisamos fazer é logar para poder ter acesso e ja vamos logar utilizando o root token

```bash
❯ vault login vaultvault
Success! You are now authenticated. The token information displayed below
is already stored in the token helper. You do NOT need to run "vault login"
again. Future Vault requests will automatically use this token.

#observer que ele ja mostrou que temos um token fixo e as polices que esse token possue
Key                  Value
---                  -----
token                vaultvault
token_accessor       7ZcQXzFupFF2BfSLPoIn6MR2
token_duration       ∞
token_renewable      false
token_policies       ["root"]
identity_policies    []
policies             ["root"]
```

Aproveite o auto complete com o tab para auto completar os comandos seguintes.

---

Quando se faz o login ele guarda o token em ~/.vault-token porém se uma variavel de ambiente for colocado o token, ele dará preferencia na variavel de ambiente VAULT_TOKEN

ex:

```bash
export VAULT_TOKEN="vaultvault" 
```

---

## list put get

```bash
#usando tab para ver o que temos
 vault list 
cubbyhole/  identity/   secret/     sys/

#aqui ele listou os possiveis path que recebem a chave

❯ vault list cubbyhole/
No value found at cubbyhole

#O path secret/ nao vem habilitado por default só vem por que estamos em -dev
❯ vault list secret/   
WARNING! The following warnings were returned from Vault:

  * Invalid path for a versioned K/V secrets engine. See the API docs for
  the appropriate API endpoints to use. If using the Vault CLI, use 'vault kv
  list' for this operation.


❯ vault kv list secret/
No value found at secret/metadata
```

Vamos colocar uma chave em secret/

```bash
❯ vault kv put secret/david value=devops 
== Secret Path ==
secret/data/david

======= Metadata =======
Key                Value
---                -----
created_time       2023-01-03T01:04:33.380084016Z
custom_metadata    <nil>
deletion_time      n/a
destroyed          false
version            1

####################################################################
# Para recuperar um valor usamos o get passando path
❯ vault kv get secret/david  
== Secret Path ==
secret/data/david

======= Metadata =======
Key                Value
---                -----
created_time       2023-01-03T01:04:33.380084016Z
custom_metadata    <nil>
deletion_time      n/a
destroyed          false
version            1

==== Data ====
Key      Value
---      -----
value    devops

##################################################################
# Se vc passar o mesmo path para um novo put ele vai sobreescrever

❯ vault kv put secret/david value=devsecops
== Secret Path ==
secret/data/david

======= Metadata =======
Key                Value
---                -----
created_time       2023-01-03T01:08:54.397937386Z
custom_metadata    <nil>
deletion_time      n/a
destroyed          false
# observe que agora ele mudou a versao para 2, isso significa que é a segunda vez que sobreescreve essa chave
version            2

❯ vault kv get secret/david                
== Secret Path ==
secret/data/david

======= Metadata =======
Key                Value
---                -----
created_time       2023-01-03T01:08:54.397937386Z
custom_metadata    <nil>
deletion_time      n/a
destroyed          false
version            2

==== Data ====
Key      Value
---      -----
value    devsecops

#################################################################
# um outro detalhe é que dentro do mesmo path podemos ter um conjunto de chave valor

❯ vault kv put secret/david so=linux alias=dpp
== Secret Path ==
secret/data/david

======= Metadata =======
Key                Value
---                -----
created_time       2023-01-03T01:12:16.709358393Z
custom_metadata    <nil>
deletion_time      n/a
destroyed          false
#observe que passou para versao 3
version            3

❯ vault kv get secret/david
== Secret Path ==
secret/data/david

======= Metadata =======
Key                Value
---                -----
created_time       2023-01-03T01:12:16.709358393Z
custom_metadata    <nil>
deletion_time      n/a
destroyed          false
version            3

# observe que temos os as duas chaves com os dois valores, porém a chave anterior não existe mais.
==== Data ====
Key      Value
---      -----
alias    dpp
so       linux

# Podemos concluir que na verdade o path é um map (dictionary) que sempre é sobreescrito pelo novo conjunto

# Se quiser recuperar somente o campo ao inves de tudo podemos usar o -field. Caso a versão nao for passada ele pega da ultima versão

❯ vault kv get -field=so secret/david
linux

❯ vault kv get -field=alias secret/david
dpp


# passando um campo que nao existe ultima versão
❯ vault kv get -field=value secret/david
Field "value" not present in secret

# Porem em alguma versão tinhamos esse campo.

##############################################################
# Conferindo as versoes

❯ vault kv metadata get secret/david
=== Metadata Path ===
secret/metadata/david

========== Metadata ==========
Key                     Value
---                     -----
cas_required            false
# quando foi criado
created_time            2023-01-03T01:04:33.380084016Z
current_version         3
custom_metadata         <nil>
delete_version_after    0s
# numero maximo de versoes que vc consegue setar
max_versions            0
oldest_version          0
# ultima atualizacao
updated_time            2023-01-03T01:12:16.709358393Z

# Veja que ele mostra atualizacoes da chave
====== Version 1 ======
Key              Value
---              -----
created_time     2023-01-03T01:04:33.380084016Z
deletion_time    n/a
destroyed        false

====== Version 2 ======
Key              Value
---              -----
created_time     2023-01-03T01:08:54.397937386Z
deletion_time    n/a
destroyed        false

====== Version 3 ======
Key              Value
---              -----
created_time     2023-01-03T01:12:16.709358393Z
deletion_time    n/a
destroyed        false


#################################################################
# Pegando o valor da versao 1
❯ vault kv get -version=1 secret/david
== Secret Path ==
secret/data/david

======= Metadata =======
Key                Value
---                -----
created_time       2023-01-03T01:04:33.380084016Z
custom_metadata    <nil>
deletion_time      n/a
destroyed          false
version            1

==== Data ====
Key      Value
---      -----
value    devops

# Da versao 2
❯ vault kv get -version=3 secret/david
== Secret Path ==
secret/data/david

======= Metadata =======
Key                Value
---                -----
created_time       2023-01-03T01:12:16.709358393Z
custom_metadata    <nil>
deletion_time      n/a
destroyed          false
version            3

==== Data ====
Key      Value
---      -----
alias    dpp
so       linux

# e só co field value que tinhamos na versao 2
❯ vault kv get -version=2 -field=value secret/david
devsecops


# para listar
❯ vault kv list secret/
Keys
----
david
```

## delete, undelete e destroy

Podemos remover todo o um path ou somente uma versão dele

```bash

❯ vault kv delete -versions 1 secret/david
Success! Data deleted (if it existed) at: secret/data/david

# Vamos ver o que acontece com o metadata para ver se sumiu
❯ vault kv metadata get secret/david             
=== Metadata Path ===
secret/metadata/david

========== Metadata ==========
Key                     Value
---                     -----
cas_required            false
created_time            2023-01-03T01:04:33.380084016Z
current_version         3
custom_metadata         <nil>
delete_version_after    0s
max_versions            0
oldest_version          0
updated_time            2023-01-03T01:12:16.709358393Z

# Não sumiu, porém....
====== Version 1 ======
Key              Value
---              -----
created_time     2023-01-03T01:04:33.380084016Z
# deletion_time que antes era n/a agora tem hora de delecão
deletion_time    2023-01-03T01:33:33.263843906Z
# porem ele nao foi destruido, isso significa que conseguimos recuperar?
destroyed        false

====== Version 2 ======
Key              Value
---              -----
created_time     2023-01-03T01:08:54.397937386Z
deletion_time    n/a
destroyed        false

====== Version 3 ======
Key              Value
---              -----
created_time     2023-01-03T01:12:16.709358393Z
deletion_time    n/a
destroyed        false

# Vamos tentar ver o que temos depois deletar
❯ vault kv get -version 1 secret/david
== Secret Path ==
secret/data/david

# trouxe o metadata mas nao trouxe o valor
======= Metadata =======
Key                Value
---                -----
created_time       2023-01-03T01:04:33.380084016Z
custom_metadata    <nil>
deletion_time      2023-01-03T01:33:33.263843906Z
destroyed          false
version            1

####################################################
# Vamos recuperar com o undelete

❯ vault kv undelete -versions 1 secret/david
Success! Data written to: secret/undelete/david

❯ vault kv get -version 1 secret/david      
== Secret Path ==
secret/data/david

# Agora ele tem o valor novamente depois do metadata
======= Metadata =======
Key                Value
---                -----
created_time       2023-01-03T01:04:33.380084016Z
custom_metadata    <nil>
deletion_time      n/a
destroyed          false
version            1

==== Data ====
Key      Value
---      -----
value    devops
```

Para destruir mesmo um uma versão

```bash
❯ vault kv destroy -versions 1 secret/david
Success! Data written to: secret/destroy/david

❯ vault kv metadata get secret/david        
=== Metadata Path ===
secret/metadata/david

========== Metadata ==========
Key                     Value
---                     -----
cas_required            false
created_time            2023-01-03T01:04:33.380084016Z
current_version         3
custom_metadata         <nil>
delete_version_after    0s
max_versions            0
oldest_version          0
updated_time            2023-01-03T01:12:16.709358393Z

====== Version 1 ======
Key              Value
---              -----
created_time     2023-01-03T01:04:33.380084016Z
deletion_time    n/a
# ele nao tem deletion time, mas tem um destroyed = true
# se ele tivesse deletado antes para depois destruir iria aparecer?
destroyed        true

====== Version 2 ======
Key              Value
---              -----
created_time     2023-01-03T01:08:54.397937386Z
deletion_time    n/a
destroyed        false

====== Version 3 ======
Key              Value
---              -----
created_time     2023-01-03T01:12:16.709358393Z
deletion_time    n/a
destroyed        false

## Vamos deletar depois destruir a versao 2

 vault kv delete -versions 2 secret/david
Success! Data deleted (if it existed) at: secret/data/david

❯ vault kv destroy -versions 2 secret/david
Success! Data written to: secret/destroy/david


❯ vault kv metadata get secret/david      
=== Metadata Path ===
secret/metadata/david

========== Metadata ==========
Key                     Value
---                     -----
cas_required            false
created_time            2023-01-03T01:04:33.380084016Z
current_version         3
custom_metadata         <nil>
delete_version_after    0s
max_versions            0
oldest_version          0
updated_time            2023-01-03T01:12:16.709358393Z

====== Version 1 ======
Key              Value
---              -----
created_time     2023-01-03T01:04:33.380084016Z
deletion_time    n/a
destroyed        true

====== Version 2 ======
Key              Value
---              -----
# veja que nesse caso agora tem os dois
created_time     2023-01-03T01:08:54.397937386Z
deletion_time    2023-01-03T01:45:05.078081528Z
destroyed        true

====== Version 3 ======
Key              Value
---              -----
created_time     2023-01-03T01:12:16.709358393Z
deletion_time    n/a
destroyed        false
```

>Uma diferenca existe no uso da engine do kv. Na versao 1 (nao estamos falando do versions) ele não habilita o versionamento da chaves, na versao 2 habilita. Isso é possivel ser executado por chave.

<https://developer.hashicorp.com/vault/docs/secrets/kv/kv-v1>

Vamos entender melhor....

```bash

❯ vault secrets enable -version=1 kv
Success! Enabled the kv secrets engine at: kv/

❯ vault list kv/
No value found at kv

❯ vault kv put kv/david terraform=ok
Success! Data written to: kv/david

❯ vault kv get kv/david
# Veja que não apareceu o metadata e as versoes
====== Data ======
Key          Value
---          -----
terraform    ok

❯ vault kv metadata get kv/david
Metadata not supported on KV Version 1

# Fazendo o upgrade para a versão 2                                               
❯ vault kv enable-versioning kv/        
Success! Tuned the secrets engine at: kv/

❯ vault kv metadata get kv/david        
= Metadata Path =
kv/metadata/david

========== Metadata ==========
Key                     Value
---                     -----
cas_required            false
created_time            2023-01-03T02:03:12.872642836Z
current_version         1
custom_metadata         <nil>
delete_version_after    0s
max_versions            0
oldest_version          1
updated_time            2023-01-03T02:03:12.872642836Z

====== Version 1 ======
Key              Value
---              -----
created_time     2023-01-03T02:03:12.872642836Z
deletion_time    n/a
destroyed        false

# Para desabilitar esse path
❯ vault secrets disable kv 
Success! Disabled the secrets engine (if it existed) at: kv/

❯ vault list kv/          
No value found at kv
```

> `Resumindo, nem perde tempo na versao=1`

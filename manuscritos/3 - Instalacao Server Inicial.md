# Instalacao Server Simples

Para nosso primeiro laboratório vamos instalar o vault de forma simples, em uma vm.

Na aws crie um ec2 com ubuntu;

Escolha um ami do ubuntu
EC2 pode ser uma t2 micro que será suficiente
Network precisa estar em uma subnet publica dentro de uma vpc
Gere uma key caso nao tenha ou escolha a que vc já possui
Pode manter o padrão de 8gb de hd que é suficiente para a brincadeira
Renomeie o security group para ficar facil deletar depois
Lembrando de liberar o ssh

![Ubuntu](../pics/ec2uuntu.png)
![Ubuntu2](../pics//ec2ubuntu2.png)

Abra dois terminais e deixa os dois acessados na máquina. Vamos depois ter 2 acessos ssh na mesma máquina. A partir de agora eu vou tagear o ssh1 e ssh2.

Na aba connect vai ter o acesso uma linha que já mostra o acesso, mas geralmente é algo tipo isso.

```bash
ssh -i "studio-20221230022119620200000002.pem" ubuntu@ec2-18-236-71-160.us-west-2.compute.amazonaws.com

```

> SSH 1

Já dentro da vm veja que o padrão é o mesmo da instalcão, mas vamos inicializar como server.

```bash
ubuntu@ip-10-192-35-91:~$ sudo su -
# Vamos coloar um nome na máquina de vault01
root@ip-10-192-35-91:~# echo "vault01" > /etc/hostname
root@ip-10-192-35-91:~# cat /etc/hostname
vault01
#saida do root
exit

# Faca um update dos repos para 
sudo apt update && sudo apt install gpg
wget -O- https://apt.releases.hashicorp.com/gpg | gpg --dearmor | sudo tee /usr/share/keyrings/hashicorp-archive-keyring.gpg >/dev/null
gpg --no-default-keyring --keyring /usr/share/keyrings/hashicorp-archive-keyring.gpg --fingerprint
echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] https://apt.releases.hashicorp.com $(lsb_release -cs) main" | sudo tee /etc/apt/sources.list.d/hashicorp.list
sudo apt update && sudo apt install vault
```

Agora vamos inicializar o servidor
Antes de inicializar vamos entender que na inicializacão o vault gera o root token que é um acesso master ao vault com politica para fazer tudo.

Além disso o vault vem no modo sealed (lacrado) e é necessário gerar as chaves para fazer o unsealed (deslacre). Nesse caso todo o dado é criptografado e em algum momento da instalacão é necessário gerar as chaves para ele fazer a descriptografia quando necessário. Um conjunto de chaves forma a chave master para fazer o unsealed.

```bash
# Para saber sobre o subcomando server execute
vault server --help
# Veja que existem várias flags que podemos passar direto na inicializacão do server, mas iremos utilizar duas para facilitar 
# -dev vai nos ajudar a não precisar de um storage backend (database) externo, guadando as chaves em memória. Além disso o cluster já vem unsealed

ubuntu@ip-10-10-10-13:~$ vault server -dev
# Abaixo a saída com alguns comentários de aprendizado
==> Vault server configuration:

# Endereco api server, será necessário mudar mais pra frente para expor
             Api Address: http://127.0.0.1:8200
                     Cgo: disabled
# Quando esta em um cluster com HA enable utiliza-se a porta 8201
         Cluster Address: https://127.0.0.1:8201
              Go Version: go1.19.3

# O tls esta disable estando somente com o http
              Listener 1: tcp (addr: "127.0.0.1:8200", cluster address: "127.0.0.1:8201", max_request_duration: "1m30s", max_request_size: "33554432", tls: "disabled")
               Log Level: info
                   Mlock: supported: true, enabled: false
           Recovery Mode: false
# Observe que o storage esta em memoria por conta do -dev
                 Storage: inmem
                 Version: Vault v1.12.2, built 2022-11-23T12:53:46Z
             Version Sha: 415e1fe3118eebd5df6cb60d13defdc01aa17b03

==> Vault server started! Log data will stream in below:

2023-01-02T21:40:39.545Z [INFO]  proxy environment: http_proxy="" https_proxy="" no_proxy=""
2023-01-02T21:40:39.545Z [WARN]  no `api_addr` value specified in config or in VAULT_API_ADDR; falling back to detection if possible, but this value should be manually set
2023-01-02T21:40:39.557Z [INFO]  core: Initializing version history cache for core
2023-01-02T21:40:39.560Z [INFO]  core: security barrier not initialized
2023-01-02T21:40:39.560Z [INFO]  core: security barrier initialized: stored=1 shares=1 threshold=1
2023-01-02T21:40:39.561Z [INFO]  core: post-unseal setup starting
2023-01-02T21:40:39.583Z [INFO]  core: loaded wrapping token key
2023-01-02T21:40:39.583Z [INFO]  core: Recorded vault version: vault version=1.12.2 upgrade time="2023-01-02 21:40:39.583267588 +0000 UTC" build date=2022-11-23T12:53:46Z
2023-01-02T21:40:39.584Z [INFO]  core: successfully setup plugin catalog: plugin-directory=""
2023-01-02T21:40:39.584Z [INFO]  core: no mounts; adding default mount table
2023-01-02T21:40:39.590Z [INFO]  core: successfully mounted backend: type=cubbyhole version="" path=cubbyhole/
2023-01-02T21:40:39.593Z [INFO]  core: successfully mounted backend: type=system version="" path=sys/
2023-01-02T21:40:39.597Z [INFO]  core: successfully mounted backend: type=identity version="" path=identity/
2023-01-02T21:40:39.601Z [INFO]  core: successfully enabled credential backend: type=token version="" path=token/ namespace="ID: root. Path: "
2023-01-02T21:40:39.601Z [INFO]  core: restoring leases
2023-01-02T21:40:39.602Z [INFO]  rollback: starting rollback manager
2023-01-02T21:40:39.603Z [INFO]  expiration: lease restore complete
2023-01-02T21:40:39.603Z [INFO]  identity: entities restored
2023-01-02T21:40:39.603Z [INFO]  identity: groups restored
2023-01-02T21:40:40.371Z [INFO]  core: post-unseal setup complete
2023-01-02T21:40:40.372Z [INFO]  core: root token generated
2023-01-02T21:40:40.372Z [INFO]  core: pre-seal teardown starting
2023-01-02T21:40:40.372Z [INFO]  rollback: stopping rollback manager
2023-01-02T21:40:40.372Z [INFO]  core: pre-seal teardown complete
2023-01-02T21:40:40.372Z [INFO]  core.cluster-listener.tcp: starting listener: listener_address=127.0.0.1:8201
2023-01-02T21:40:40.372Z [INFO]  core.cluster-listener: serving cluster requests: cluster_listen_address=127.0.0.1:8201
2023-01-02T21:40:40.373Z [INFO]  core: post-unseal setup starting
2023-01-02T21:40:40.373Z [INFO]  core: loaded wrapping token key
2023-01-02T21:40:40.373Z [INFO]  core: successfully setup plugin catalog: plugin-directory=""
2023-01-02T21:40:40.373Z [INFO]  core: successfully mounted backend: type=system version="" path=sys/
2023-01-02T21:40:40.374Z [INFO]  core: successfully mounted backend: type=identity version="" path=identity/
2023-01-02T21:40:40.374Z [INFO]  core: successfully mounted backend: type=cubbyhole version="" path=cubbyhole/
2023-01-02T21:40:40.375Z [INFO]  core: successfully enabled credential backend: type=token version="" path=token/ namespace="ID: root. Path: "
2023-01-02T21:40:40.375Z [INFO]  core: restoring leases
2023-01-02T21:40:40.376Z [INFO]  expiration: lease restore complete
2023-01-02T21:40:40.376Z [INFO]  rollback: starting rollback manager
2023-01-02T21:40:40.376Z [INFO]  identity: entities restored
2023-01-02T21:40:40.376Z [INFO]  identity: groups restored
2023-01-02T21:40:40.376Z [INFO]  core: post-unseal setup complete
2023-01-02T21:40:40.376Z [INFO]  core: vault is unsealed
2023-01-02T21:40:40.379Z [INFO]  core: successful mount: namespace="" path=secret/ type=kv version=""
2023-01-02T21:40:40.382Z [INFO]  secrets.kv.kv_9b6b5674: collecting keys to upgrade
2023-01-02T21:40:40.382Z [INFO]  secrets.kv.kv_9b6b5674: done collecting keys: num_keys=1
2023-01-02T21:40:40.382Z [INFO]  secrets.kv.kv_9b6b5674: upgrading keys finished
# Veja que ele deu um warning dizendo que esta em memoria, o root token ja esta criado e existe somente 1 chave para deslacre do vault
WARNING! dev mode is enabled! In this mode, Vault runs entirely in-memory
and starts unsealed with a single unseal key. The root token is already
authenticated to the CLI, so you can immediately begin using Vault.

You may need to set the following environment variables:

# Faca o export dessa variavel em outro terminal para usar o vault agora para acessar o server, o mesmo binario inclusive

    $ export VAULT_ADDR='http://127.0.0.1:8200'

The unseal key and root token are displayed below in case you want to
seal/unseal the Vault or re-authenticate.

#A chave de unseal e o token root já criados
# Deve ser guardado em um local seguro
Unseal Key: hDqV7A9L4+Us9g4exZgagzZDvbOAtaDVcfiSMe50WiM=
Root Token: hvs.ZFzL59ksn7aIVObxxPCsHYvr


# VEJA ESTA LINHA, NAO USE O ROOT TOKEN EM PRODUCAO
Development mode should NOT be used in production installations!
```

Observe que o servico ficou rodando e travou o terminal, para para o outro ssh

>SSH 2

```bash
ubuntu@ip-10-10-10-13:~$ export VAULT_ADDR='http://127.0.0.1:8200'
ubuntu@ip-10-10-10-13:~$ vault status
Key             Value
---             -----
Seal Type       shamir
Initialized     true
# Como estamos inicializamos em -dev ele veio nao selado
Sealed          false
# Quantas chaves para unseal eu tenho
Total Shares    1
# Quantas chaves precisa para tirar o lacre do cluster
# É possivel ter 5 chaves por exemplo e usar somente a combinacao de 2 para realizar o unseal
Threshold       1
Version         1.12.2
Build Date      2022-11-23T12:53:46Z
Storage Type    inmem
Cluster Name    vault-cluster-8a5fcbf7
Cluster ID      c5fe4d4b-9d8c-26aa-abbd-9bd7e6353ae5
# Mostra o flag de alta disponibilidade esta ou nao ativado
HA Enabled      false
ubuntu@ip-10-10-10-13:~$ 
```

## Personalizando um pouco mais a instalacão

>SSH 1

Faca um control c e pare a execucão do vault, pois vamos subir com algumas pernalizacões. Vamos expor o nosso vault ppara respondar em outra url e criar um token personalizado.

```bash
vault server -dev -dev-listen-address=0.0.0.0:8200 -dev-root-token-id=vaultvault

...
You may need to set the following environment variables:

    $ export VAULT_ADDR='http://0.0.0.0:8200'

The unseal key and root token are displayed below in case you want to
seal/unseal the Vault or re-authenticate.

Unseal Key: IaWAJt1g5yXDhXbDJDWYBiRva8LjB49qGxpQ0teq3iM=
Root Token: vaultvault

Development mode should NOT be used in production installations!
```

Agora na sua máquina local se vc tiver o vault instalado... e podemos observar que mudamos o root token para vaultvault

```bash
❯ export VAULT_ADDR='http://ec2-18-236-71-160.us-west-2.compute.amazonaws.com:8200'

~                                                                                                                                      1.3.3 20:40:41
❯ vault status                                                                     
Key             Value
---             -----
Seal Type       shamir
Initialized     true
Sealed          false
Total Shares    1
Threshold       1
Version         1.12.2
Build Date      2022-11-23T12:53:46Z
Storage Type    inmem
Cluster Name    vault-cluster-5700a368
Cluster ID      50edc99e-ed17-746a-cb11-3097450429af
HA Enabled      false
```

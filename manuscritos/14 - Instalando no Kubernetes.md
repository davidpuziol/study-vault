# Instalando no Kubernetes

Para instalar no kubernetes vamos seguir a documentação <https://developer.hashicorp.com/vault/docs/platform/k8s/helm> e usar o helm.

Tenha o helm instalado e um cluster kubernetes pronto para execução. Esse exemplo estarei utilizando um cluster k3d em minha própria máquina.

```bash
helm repo add hashicorp https://helm.releases.hashicorp.com

# Vamos fazer o download do values para configurar o nosso chart.
helm show values hashicorp/vault > ./resources/Values.yaml
# Fiz algumas alterações no Values.yaml para utilizar o storage backend raft
helm install vault hashicorp/vault  --values ./resources/Values.yaml --create-namespace --namespace vault

❯ k get pods -n vault
NAME                                    READY   STATUS    RESTARTS   AGE
vault-1                                 0/1     Running   0          2m1s
vault-agent-injector-68bb8c4b57-rgtn9   1/1     Running   0          2m1s
vault-0                                 0/1     Running   0          2m1s
vault-2                                 0/1     Running   0          2m1s
```

Agora precisamos iniciar nosso vault e tb precisamos fazer o unseal

```bash
❯ kubectl exec -ti vault-0 -- vault operator init
# Unseal Key 1: isSKqtCxxmEDhwUTma023mN6CfNk9jUzw8a43Dc3ryNn
# Unseal Key 2: 7mqnYzShH57VmKY5MIJzUgE7XunZFTUsP4kEmNzfDopt
# Unseal Key 3: R61rcxDiVOFemzab64EgO4Klaw5ubO2rcFNwBI4b1XxT
# Unseal Key 4: dLFA9QbNItqfNkSy6DuvZ0g4rWMbaJBtuCAgTM4qXcHU
# Unseal Key 5: AP+pVQXnkLWRMoYBNpNntB/abjxERnMG69xuiZ1wsV74

# Initial Root Token: hvs.YLs0XN4yIfTOkn3VliDKsxkg

# Vault initialized with 5 key shares and a key threshold of 3. Please securely
# distribute the key shares printed above. When the Vault is re-sealed,
# restarted, or stopped, you must supply at least 3 of these keys to unseal it
# before it can start servicing requests.

# Vault does not store the generated root key. Without at least 3 keys to
# reconstruct the root key, Vault will remain permanently sealed!

# It is possible to generate new unseal keys, provided you have a quorum of
# existing unseal keys shares. See "vault operator rekey" for more information.

# Precisamos fazer utilizando as chaves acima
❯ kubectl exec -ti vault-0 -- vault operator unseal isSKqtCxxmEDhwUTma023mN6CfNk9jUzw8a43Dc3ryNn
❯ kubectl exec -ti vault-0 -- vault operator unseal 7mqnYzShH57VmKY5MIJzUgE7XunZFTUsP4kEmNzfDopt
❯ kubectl exec -ti vault-0 -- vault operator unseal R61rcxDiVOFemzab64EgO4Klaw5ubO2rcFNwBI4b1XxT
# Unseal Key (will be hidden): 
# Key                     Value
# ---                     -----
# Seal Type               shamir
# Initialized             true
# Sealed                  false
# Total Shares            5
# Threshold               3
# Version                 1.12.1
# Build Date              2022-10-27T12:32:05Z
# Storage Type            raft
# Cluster Name            vault-cluster-345b8abb
# Cluster ID              e2c87313-eef8-6f50-57bd-371315510f89
# HA Enabled              true
# HA Cluster              https://vault-0.vault-internal:8201
# HA Mode                 active
# Active Since            2023-01-07T10:47:58.827252157Z
# Raft Committed Index    36
# Raft Applied Index      36
```

E por fim temos que fazer os pods vault-1 e vault 2 se juntar ao cluster

```bash
kubectl exec -ti vault-1 -- vault operator raft join http://vault-0.vault-internal:8200

❯ kubectl exec -n vault -ti vault-1 -- vault operator unseal isSKqtCxxmEDhwUTma023mN6CfNk9jUzw8a43Dc3ryNn
❯ kubectl exec -n vault -ti vault-1 -- vault operator unseal 7mqnYzShH57VmKY5MIJzUgE7XunZFTUsP4kEmNzfDopt
❯ kubectl exec -n vault -ti vault-1 -- vault operator unseal R61rcxDiVOFemzab64EgO4Klaw5ubO2rcFNwBI4b1XxT

❯ kubectl exec -ti vault-2 -- vault operator raft join http://vault-0.vault-internal:8200

❯ kubectl exec -n vault -ti vault-2 -- vault operator unseal isSKqtCxxmEDhwUTma023mN6CfNk9jUzw8a43Dc3ryNn
❯ kubectl exec -n vault -ti vault-2 -- vault operator unseal 7mqnYzShH57VmKY5MIJzUgE7XunZFTUsP4kEmNzfDopt
❯ kubectl exec -n vault -ti vault-2 -- vault operator unseal R61rcxDiVOFemzab64EgO4Klaw5ubO2rcFNwBI4b1XxT
```

Para verificar se foi inicializado com sucesso utilize o root token

```bash
kubectl exec -n vault -ti vault-0 -- vault login hvs.YLs0XN4yIfTOkn3VliDKsxkg

kubectl exec -ti vault-0 -- vault operator raft list-peers
# Node                                    Address                        State       Voter
# ----                                    -------                        -----       -----
# a1799962-8711-7f28-23f0-cea05c8a527d    vault-0.vault-internal:8201    leader      true
# e6876c97-aaaa-a92e-b99a-0aafab105745    vault-1.vault-internal:8201    follower    true
# 4b5d7383-ff31-44df-e008-6a606828823b    vault-2.vault-internal:8201    follower    true
```

# Auto Unseal e Auto Init

O desbloqueio do vault deve acontecer toda vez que o serviço vault for iniciado ou quando ele for trancado propositalmente.

Chave raiz gerada pelo cofre (não confunda com token raiz) e usa um algoritmo Shamir's Secret Sharing para dividir a chave em pedaços. Durante a inicialização, podemos determinar quantos compartilhamentos de chave serão necessários para abrir o Vault. Essa é uma opção independente de nuvem e muito flexível, mas pode se tornar dolorosa quando você tem muitos clusters do Vault, muitas chaves e muitos detentores de chaves.

![unsealkeys](../pics/unsealkeys.png)

Podemos definir 3 métodos:

- Abertura manual
  - Em cada um dos servers precisa ser executado essa operação manualmente. Já foi executada várias vezes durante o estudo.
- Abertura automática
  - Efetuada automáticamente em todos os servers sempre que o serviço for iniciado.
- Transit Unseal
  - Quando a abertura de um server automaticamente abre os outros.

Fazer isso manualmente é inviável.

## Auto Unseal


## Transit Unseal

O que torna esse Auto-unseal diferente é que não dependemos de um serviço externo, mas do próprio Vault externo. Desta forma, podemos colocar um Vault central que será responsável por abrir automaticamente outras instâncias do Vault.

![transitunseal](../pics/transicunseal.png)

<https://dev.to/luafanti/vault-auto-unseal-using-transit-secret-engine-on-kubernetes-13k8
# Métodos de autenticação

Chega de usar o root token, pois ele não foi feito para user usado pra isso, Vamos aprender outros métodos de autenticação que irão gerar os tokens para cada usuário. Faça o sign out na interface gráfica.

Para trabalhar com autenticação o comando é `vault auth`.

```bash
❯ vault auth --help
Usage: vault auth <subcommand> [options] [args]

  This command groups subcommands for interacting with Vault's auth methods.
  Users can list, enable, disable, and get help for different auth methods.

  To authenticate to Vault as a user or machine, use the "vault login" command
  instead. This command is for interacting with the auth methods themselves, not
  authenticating to Vault.

  List all enabled auth methods:

      $ vault auth list

  Enable a new auth method "userpass";

      $ vault auth enable userpass

  Get detailed help information about how to authenticate to a particular auth
  method:

      $ vault auth help github

  Please see the individual subcommand help for detailed usage information.

Subcommands:
    disable    Disables an auth method
    enable     Enables a new auth method
    help       Prints usage for an auth method
    list       Lists enabled auth methods
    move       Move an auth method to a new path
    tune       Tunes an auth method configuration



```

>Observe os comandos são bem parecido com os do secret.

Para listar os métodos de autenticação que temos

```bash
❯ vault auth list
Path      Type     Accessor               Description                Version
----      ----     --------               -----------                -------
token/    token    auth_token_2d7b0115    token based credentials    n/a
```

Para habilitar algum método de autenticação pode ser visto os possíveis aqui <https://developer.hashicorp.com/vault/docs/auth>.

Em cada um dos métodos possui a documentação de como você configura para sair utilizando.

É interessante alguns métodos e são bem fáceis de configurar.

- aws
- github
- kubernetes
- okta
- oidc providers
- userpass (vamos começar por esse que é o mais simples)
- etc

Habilitando o userpass <https://developer.hashicorp.com/vault/docs/auth/userpass>.

```bash
# habilitando de forma padrão
❯ vault auth enable userpass
Success! Enabled userpass auth method at: userpass/

# Habilitando outro userpass agora personalizado e com uma descrição
❯ vault secrets enable -description="AWS IAM" aws
Success! Enabled the aws secrets engine at: aws/

❯ vault secrets list                                 
Path          Type         Accessor              Description
----          ----         --------              -----------
aws/          aws          aws_bcc399b1          AWS IAM
cubbyhole/    cubbyhole    cubbyhole_7c7a7f10    per-token private secret storage
identity/     identity     identity_0dd9718b     identity store
sys/          system       system_d63da29b       system endpoints used for control, policy and debugging
```

Agora precisamos gerar um IAM user na aws para dar para o vault. Esse iam tem que ser capaz de gerar novas roles.

Do mesmo modo que as secrets podemos ter vários do mesmo tipo montados em diferentes paths.

Para desativar algum método...

```bash
❯ vault auth disable userpass/                                         
Success! Disabled the auth method (if it existed) at: userpass/

❯ vault auth list                                                      
Path        Type        Accessor                  Description                Version
----        ----        --------                  -----------                -------
myusers/    userpass    auth_userpass_55edd36e    Meus usuários              n/a
token/      token       auth_token_2d7b0115       token based credentials    n/a
```

Na pŕopria documentação mostra como criar um usuário, vamos segui-la, inclusive usando uma api se quiser.

```bash
# o caminho é auth/<meupath>/user/<novouser> password=<password_new_user> ainda é possivel passar a chave policies=<grupo_da_police> se deseja já colocar esse usuário em um grupo de policies
❯ vault write auth/myusers/users/david password=devops
Success! Data written to: auth/myusers/users/david

❯ vault list  auth/myusers/users      
Keys
----
david

# Não tem nenhuma informacação dele pois ainda não foi autenticado com ele
❯ vault read auth/myusers/users/david
Key                        Value
---                        -----
token_bound_cidrs          []
token_explicit_max_ttl     0s
token_max_ttl              0s
token_no_default_policy    false
token_num_uses             0
token_period               0s
token_policies             []
token_ttl                  0s
```

Agora vamos fazer o login com este usuário e pegar o token

```bash
# se tivesse usado a path padrao do userpass nao precisaríamos passar o path
❯ vault login -method=userpass -path=myusers username=david password=devops
Success! You are now authenticated. The token information displayed below
is already stored in the token helper. You do NOT need to run "vault login"
again. Future Vault requests will automatically use this token.

Key                    Value
---                    -----
token                  hvs.CAESIPI0Ql0FlolF9XNIMryx1a6eUOQtM_H0QQUftAbNrlF4Gh4KHGh2cy5zYjF0NnAxVk1obkJHOXZZd2hDazIxUmw
token_accessor         oRGeWCW6tJ0IM4mvvFtDuGGP
token_duration         768h
token_renewable        true
token_policies         ["default"]
identity_policies      []
policies               ["default"]
token_meta_username    david
```

Pegando este token e indo lá na gui podemos fazer autenticação agora usando ele. Veja que estamos logado com david e somente temos o cubbehole/ que é o path das secrets deste usuário.

![newuser](../pics/newuser.png)

Este usuário pode criar uma secret dentro do seu cubbehole/ mas não pode por exemplo montar uma secret engine ou ter acesso a outros path pois não tem permissão, afinal não definimos uma policy para ele.

>Na verdade quando o login é feito seja por qualquer método de autenticação ele cria um token e já autentica com esse token. É executado um processo de duas etapas por baixo dos panos.

Uma vez que fizemos o comando acima ele gerou o token e depois fez

```bash
vault login -method=token hvs.CAESIPI0Ql0FlolF9XNIMryx1a6eUOQtM_H0QQUftAbNrlF4Gh4KHGh2cy5zYjF0NnAxVk1obkJHOXZZd2hDazIxUmw
hvs.CAESIPI0Ql0FlolF9XNIMryx1a6eUOQtM_H0QQUftAbNrlF4Gh4KHGh2cy5zYjF0NnAxVk1obkJHOXZZd2hDazIxUmw
Success! You are now authenticated. The token information displayed below
is already stored in the token helper. You do NOT need to run "vault login"
again. Future Vault requests will automatically use this token.

Key                    Value
---                    -----
token                  hvs.CAESIPI0Ql0FlolF9XNIMryx1a6eUOQtM_H0QQUftAbNrlF4Gh4KHGh2cy5zYjF0NnAxVk1obkJHOXZZd2hDazIxUmw
token_accessor         oRGeWCW6tJ0IM4mvvFtDuGGP
token_duration         767h38m36s
token_renewable        true
token_policies         ["default"]
identity_policies      []
policies               ["default"]
token_meta_username    david
zsh: command not found: hvs.CAESIPI0Ql0FlolF9XNIMryx1a6eUOQtM_H0QQUftAbNrlF4Gh4KHGh2cy5zYjF0NnAxVk1obkJHOXZZd2hDazIxUmw
```

Que deu na mesma.
Agora vamos converir se ele tem acesso a outro path

```bash
# Pode listar seu próprio cubiculo de chaves mas não pode executar um comando de nível superior
❯ vault kv list cubbyhole
No value found at cubbyhole

❯ vault secrets list     
Error listing secrets engines: Error making API request.

URL: GET http://ec2-18-236-71-160.us-west-2.compute.amazonaws.com:8200/v1/sys/mounts
Code: 403. Errors:

* 1 error occurred:
        * permission denied

# Voltando para o token root criado anteriorment
vault login -method=token vaultvault                                                                                     
Success! You are now authenticated. The token information displayed below
is already stored in the token helper. You do NOT need to run "vault login"
again. Future Vault requests will automatically use this token.

Key                  Value
---                  -----
token                vaultvault
token_accessor       7ZcQXzFupFF2BfSLPoIn6MR2
token_duration       ∞
token_renewable      false
token_policies       ["root"]
identity_policies    []
policies             ["root"]

# Ja podemos listar novamente
❯ vault secrets list     
Path           Type         Accessor              Description
----           ----         --------              -----------
aws/           aws          aws_e8c5644a          Iam aws
cubbyhole/     cubbyhole    cubbyhole_7a017da3    per-token private secret storage
identity/      identity     identity_e76888ac     identity store
my_new_ssh/    ssh          ssh_443037b9          Minhas sshs
mykv/          kv           kv_b9d5e01b           n/a
secret/        kv           kv_cf90e1db           key/value secret storage
sys/           system       system_129cb239       system endpoints used for control, policy and debugging
```

Para desativar esse método de autenticação é simples e ao mesmo tempo todos os tokens dos usuário deste método são revogados automaticamente.

```bash
❯ vault auth disable myusers/    
Success! Disabled the auth method (if it existed) at: myusers/

~/Desktop/personal/study-vault main !3 ?4                                                                                                                                                                           1.3.3 00:31:35
❯ vault auth list                
Path      Type     Accessor               Description                Version
----      ----     --------               -----------                -------
token/    token    auth_token_2d7b0115    token based credentials    n/a
```

vamos só deixar o userpass criado no padrão pois vamos usar no proximo estudo.

```bash
❯ vault auth enable userpass                                              
Success! Enabled userpass auth method at: userpass/

❯ vault auth list      
Path         Type        Accessor                  Description                Version
----         ----        --------                  -----------                -------
token/       token       auth_token_2d7b0115       token based credentials    n/a
userpass/    userpass    auth_userpass_d1ec22b2    n/a                        n/a
```

> Uma dica é manter o path padrão e somente dividir dentro do próprio path. Isso evita ter que ficar passando ele o tempo todo, inclusive na interface gráfica.

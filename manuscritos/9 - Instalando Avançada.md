# Instalanção avançada

Nessa instalação não vamos mais utilizar o modo -dev então precisaremos configurar corretamente o server.

Pode ser qualquer distribuição linux, mas agora vamos fazer no ubuntu. Prepare uma nova vm com o vault em um t2.micro mesmo como na primeira instalação. Pode destruir

Delete a vm anterior e crie uma nova, que é bom pra fixar a instalação.

faça um ssh para a sua vm e vamos começar

## Instalação

```bash
sudo su -
hostname vault01
echo vault01 > /etc/hostname

# Faca um update dos repos para 
sudo apt update && sudo apt install gpg
wget -O- https://apt.releases.hashicorp.com/gpg | gpg --dearmor | sudo tee /usr/share/keyrings/hashicorp-archive-keyring.gpg >/dev/null
gpg --no-default-keyring --keyring /usr/share/keyrings/hashicorp-archive-keyring.gpg --fingerprint
echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] https://apt.releases.hashicorp.com $(lsb_release -cs) main" | sudo tee /etc/apt/sources.list.d/hashicorp.list
sudo apt update && sudo apt install vault
vault -autocomplete-install
bash
```

Agora precisamos criar uma estrutura de diretórios. Por enquanto também vamos colocar os logs dentro de uma pasta, mas poderíamos apontar um servidor de log como faremos mais pra frente.

```bash

## se não existir crie com o comando abaixo
useradd --system vault

# mantendo a consistencia do linxu, criando um local para os logs
mkdir /var/log/vault
# Por enquanto não teremos ainda um storage backend, vamos guardar as secrets em file no caso no data
mkdir /var/lib/vault
# Esta é a pasta que quando o vault inicializa ele guarda seus dados aqui. Se remover todo o conteúdo desta pasta o vault perderá sua configuração e precisará refazer um init
mkdir /var/lib/vault/data
# Fazendo um link para facilitar a vida
ln -s /var/log/vault/ /var/lib/vault/logs
mkdir /etc/vault
# Onde conterá todo o conteúdo inicial de configuracao do vault server
touch /etc/vault/config.hcl
```

O script de instalação já cria o usuário de sistema para o vault, a linha abaixo seria o que iria criar
Confira

```bash
cat /etc/passwd | grep vault
# vault:x:998:998::/home/vault:/bin/false

chown -R vault:vault /var/lib/vault
chown -R vault:vault /var/log/vault/
chown -R vault:vault /etc/vault
```

Agora vamos colocar as configurações encontradas em <https://developer.hashicorp.com/vault/docs/configuration>
>A configuração abaixo ainda não utiliza o https e somente o vault esteja rodando na vm. Ainda podemos habilitar a telemetria caso estejamos usando algum prometheus ou outro.

```bash
cat >> /etc/vault/config.hcl <<EOF
storage "file" {
  path = "/var/lib/vault/data"
}

listener "tcp" {
  address     = "0.0.0.0:8200"
  tls_disable = 1
}

api_addr = "http://0.0.0.0:8200"
ui = true
cluster_name = "vault"
disable_mlock = true
max_lease_ttl = "12h"
default_lease_ttl = "6h"
pid_file = "/var/lib/vault/vault.pid"
EOF
```

chown -R vault:vault /etc/vault

agora vamos criar o service para levantar o vault como serviço. Procure no google melhorias para esse serviço pois tem muitos exemplo.

```bash
cat >> /etc/systemd/system/vault.service <<EOF
[Unit]
Description = Vault Server
Requires = network-online.target
After = network-online.target
ConditionFileNotEmpty = /etc/vault/config.hcl

[Service]
User = vault
Group = vault
Restart = on-failure
ExecStart = /usr/bin/vault server -config=/etc/vault/config.hcl
StandardOutput = append:/var/log/vault/output.log
StandardError = append:/var/log/vault/error.log
ExecReload = /bin/kill -HUP 
KillSignal = SIGTERM
LimitMEMLOCK = infinity
[Install]
WantedBy = multi-user.target
#Environment="VAULT_ADDR="http://127.0.0.1:8200"
EOF

# vamos levantar o serviço
systemctl daemon-reload
systemctl enable vault.service
systemctl status vault.service

# Já vamos facilitar para que o root sempre tenha a variavel que aponta para o server exportada
echo "export VAULT_ADDR='http://127.0.0.1:8200'" >> /root/.bashrc

ss -atump | grep vault
# tcp   LISTEN 0      4096            0.0.0.0:8200           0.0.0.0:*     users:(("vault",pid=2837,fd=8))
```

## Inicializando o Vault

Observe que no serviço já colocamos para levantar a variavel de ambiente VAULT_ADD para ele mesmo.
Agora precisamo inicializar o server para gerar os tokens e as chaves para unseal.

```bash
vault status

# Key                Value
# ---                -----
# Seal Type          shamir
# # Observe que ele não veio inicializado e veio sealed, diferente do modo -dev que nos traz essa facilidade
# Initialized        false
# Sealed             true
# Total Shares       0
# Threshold          0
# Unseal Progress    0/0
# Unseal Nonce       n/a
# Version            1.12.2
# Build Date         2022-11-23T12:53:46Z
# Storage Type       file
# HA Enabled         false
```

Agora vamos inicializar o nosso cluster. vault operator é o cara que trabalha em cima disso e muitas outras coisas que veremos mais pra frente.

>Guarde essas chaves em um lugar seguro e não dentro do próprio server.

```bash
vault operator init -key-shares=3 -key-threshold=2 | tee /var/lib/vault/initvault.txt
# Unseal Key 1: rdlvzWl3+YfTN+t6wuf9eFkH3vjR+MK7bNWI9c/HNHnm
# Unseal Key 2: sIghQ+O59AUt5l8vD1XjkgJvONdaE34weE+VBkEnTRn6
# Unseal Key 3: FVnSbdjcBRS0oSDrI7gPSn6m3wmvBSvFOSAwbm9SHvqR

# Initial Root Token: hvs.xUt6nh0d9RWGhxhvec5LQOhq

# Vault initialized with 3 key shares and a key threshold of 2. Please securely
# distribute the key shares printed above. When the Vault is re-sealed,
# restarted, or stopped, you must supply at least 2 of these keys to unseal it
# before it can start servicing requests.

# Vault does not store the generated root key. Without at least 2 keys to
# reconstruct the root key, Vault will remain permanently sealed!

# It is possible to generate new unseal keys, provided you have a quorum of
# existing unseal keys shares. See "vault operator rekey" for more information.
# root@vault01:/var/lib/vault# ls
# data  initvault.txt  logs  vault.pid

vault status
# Key                Value
# ---                -----
# Seal Type          shamir
# Initialized        true
# Sealed             true
# Total Shares       3
# Threshold          2
# Unseal Progress    0/2
# Unseal Nonce       n/a
# Version            1.12.2
# Build Date         2022-11-23T12:53:46Z
# Storage Type       file
# HA Enabled         false

# Agora vemos que temos um cluste inicializado, mas ainda selado (sealed) com um total de 5 chaves, mas precisa de 3 para destravar (threshold). Observe também que o storage type esta como file e ainda não estamos com alta disponibilidade.

# Vamos separar os arquivos das chaves para guardar depois.
cat /var/lib/vault/initvault.txt | grep "Root Token" | awk '{print $4}' | tee /var/lib/vault/root.token
cat /var/lib/vault/initvault.txt | grep "Unseal Key" | awk '{print $4}' | tee /var/lib/vault/unsealed.keys
```

## Destravando Vault

Agora vamos começar a destravar usando as chaves que temos nos arquivos. Precisamos passar 2 delas e já vamos gerar o token

```bash
vault operator unseal $(cat /var/lib/vault/unsealed.keys | awk 'NR ==1')
vault operator unseal $(cat /var/lib/vault/unsealed.keys | awk 'NR ==2')

vault status | grep Sealed

# Sealed          false
export VAULT_TOKEN=$(cat /var/lib/vault/root.token| awk 'NR ==1')
echo "export VAULT_TOKEN='$(cat /var/lib/vault/root.token| awk 'NR ==1')'" >> /root/.bashrc
```

## Gerando outro root token

Agora vamos gerar um novo root token para extra.

```bash
# O comando init já irá gerar uma senha OTP que é vinculada com o token final. Cada root token tem o seu próprio OTP (One Time Passwork).
# Se já tiver uma otp criada um outro root token, porem vinculado ao mesmo otp poderia passar o -otp. Nesse caso não temos nada.
vault operator generate-root -init | tee /var/lib/vault/generateroottoken.txt

#criando os arquivos para guardar
cat /var/lib/vault/generateroottoken.txt | grep "Nonce" | awk '{print $2}' | tee /var/lib/vault/nonce
cat /var/lib/vault/generateroottoken.txt | grep "OTP" | awk 'NR ==1'| awk '{print $2}' | tee /var/lib/vault/otpextra
# removendo o arquivo, pois já está separado
rm -rf /var/lib/vault/generateroottoken.txt

# Passando as chaves
vault operator generate-root -nonce $(cat /var/lib/vault/nonce) $(cat /var/lib/vault/unsealed.keys | awk 'NR ==1')
vault operator generate-root -nonce $(cat /var/lib/vault/nonce) $(cat /var/lib/vault/unsealed.keys | awk 'NR ==2') | grep "Encoded Token" | awk '{print $3}' | tee /var/lib/vault/encoded.token

vault operator generate-root -decode $(cat /var/lib/vault/encoded.token) -otp $(cat /var/lib/vault/otpextra) >>/var/lib/vault/root.token

# Não precisaremos mais do nonce pois ele é como se fosse uma sessão e esta ja acabou.
rm -rf /var/lib/vault/nonce

```

## Checkup da Instalação

Fazendo um checkup vemos o que podemos melhorar para atender todos os requisitos para uma instalação perfeita. Ainda não é o caso, pois estamos melhorando as coisas.

```bash
vault operator diagnose -config /etc/vault/config.hcl
# Vault v1.12.2 (415e1fe3118eebd5df6cb60d13defdc01aa17b03), built 2022-11-23T12:53:46Z

# Results:
# [ failure ] Vault Diagnose: HCP link check will not run on OSS Vault.
#   [ warning ] Check Operating System
#     [ success ] Check Open File Limits: Open file limits are set to 1048576.
#     [ success ] Check Disk Usage: / usage ok.
#     [ warning ] Check Disk Usage: /snap/amazon-ssm-agent/6312 is 100.00 percent full.
#       It is recommended to have more than five percent of the partition free.
#     [ warning ] Check Disk Usage: /snap/core18/2632 is 100.00 percent full.
#       It is recommended to have more than five percent of the partition free.
#     [ warning ] Check Disk Usage: /snap/core20/1695 is 100.00 percent full.
#       It is recommended to have more than five percent of the partition free.
#     [ warning ] Check Disk Usage: /snap/lxd/23541 is 100.00 percent full.
#       It is recommended to have more than five percent of the partition free.
#     [ warning ] Check Disk Usage: /snap/snapd/17883 is 100.00 percent full.
#       It is recommended to have more than five percent of the partition free.
#     [ warning ] Check Disk Usage: /snap/core18/2667 is 100.00 percent full.
#       It is recommended to have more than five percent of the partition free.
#     [ warning ] Check Disk Usage: /snap/core20/1778 is 100.00 percent full.
#       It is recommended to have more than five percent of the partition free.
#   [ success ] Parse Configuration
#   [ warning ] Check Telemetry: Telemetry is using default configuration
#     By default only Prometheus and JSON metrics are available.  Ignore this warning if you are using telemetry or are using these metrics and are satisfied with the default retention time and gauge period.
#   [ success ] Check Storage
#     [ success ] Create Storage Backend
#     [ success ] Check Storage Access
#   [ skipped ] Check Service Discovery: No service registration configured.
#   [ success ] Create Vault Server Configuration Seals
#   [ skipped ] Check Transit Seal TLS: No transit seal found in seal configuration.
#   [ success ] Create Core Configuration
#     [ success ] Initialize Randomness for Core
#   [ success ] HA Storage
#     [ success ] Create HA Storage Backend
#     [ skipped ] Check HA Consul Direct Storage Access: No HA storage stanza is configured.
#   [ success ] Determine Redirect Address
#   [ success ] Check Cluster Address: Cluster address is logically valid and can be found.
#   [ success ] Check Core Creation
#   [ skipped ] Check For Autoloaded License: License check will not run on OSS Vault.
#   [ failure ] Start Listeners
#     [ warning ] Check Listener TLS: Listener at address 0.0.0.0:8200: TLS is disabled in a listener config stanza.
#     [ failure ] Create Listeners: Error initializing listener of type tcp: listen tcp4 0.0.0.0:8200: bind: address already in use
#   [ skipped ] Check Autounseal Encryption: Skipping barrier encryption test. Only supported for auto-unseal.
#   [ success ] Check Server Before Runtime
#   [ success ] Finalize Shamir Seal
```

## Renovando as unseal keys

<https://developer.hashicorp.com/vault/docs/internals/rotation>

Para renovar as chaves, caso queira, é possível.

```bash
vault operator rekey -init -key-shares 3 -key-threshold 2 | tee /var/lib/vault/rekey.txt
cat /var/lib/vault/rekey.txt | grep "Nonce" | awk '{print $2}' > /var/lib/vault/nonce

vault operator rekey -nonce $(cat /var/lib/vault/nonce) $(cat /var/lib/vault/unsealed.keys | awk 'NR ==1')
vault operator rekey -nonce $(cat /var/lib/vault/nonce) $(cat /var/lib/vault/unsealed.keys | awk 'NR ==2') | tee /var/lib/vault/newunsealed.keys

cat /var/lib/vault/newunsealed.keys | grep "Key" | awk '{print $3}' > /var/lib/vault/unsealed.keys

rm -rf /var/lib/vault/nonce
rm -rf /var/lib/vault/newunsealed.keys
```

## Agora vamos habilitar um root token extra funcionalidades

```bash
# habilitando um secret engine do tipo kv com path = secret
vault secrets enable -version=2 -path=secret/ kv

# vamos habilitar também os authenticadores que vamos usar para conseguir tokens
# essa parte depende de como vc quer os paths e o que vc vai usar
vault auth enable userpass 
vault auth enable aws
```

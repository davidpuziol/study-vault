# Instalacao do CLI

## Instalando o binário em sua máquina local

A mairia dos métodos estão em <https://developer.hashicorp.com/vault/downloads> mas vou focar somente linux ubuntu.

Abaixo 4 métodos escolha um.

Antes de continuar, vale a pena entender que a instalacão do vault depois de instalado pode ser inicializado como agent ou servidor na mesma instalacão somente passando os comandos necessários. As instalacões abaixo mostram somente a instalacão do binario sem nenhuma inicializacao.

### Ubuntu

```bash
## Add key
wget -O- https://apt.releases.hashicorp.com/gpg | gpg --dearmor | sudo tee /usr/share/keyrings/hashicorp-archive-keyring.gpg
## add repo
echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] https://apt.releases.hashicorp.com $(lsb_release -cs) main" | sudo tee /etc/apt/sources.list.d/hashicorp.list
## update e install
sudo apt update && sudo apt install vault
vault --version
```

### asdf no zsh

Claro que é necessaŕio o asdf instalado siga essa documentacão <https://asdf-vm.com/guide/getting-started.html#_3-install-asdf>

com o asdf instalado

```bash
asdf plugin add vault   
asdf install vault latest
asdf global vault 1.12.2+ent
vault --version
```

### Baixando o zip e instalando

```bash
wget https://releases.hashicorp.com/vault/1.12.2/vault_1.12.2_linux_amd64.zip
sudo apt-get install unzip
unzip vault_1.12.2_linux_amd64.zip
mv vault /usr/local/bin/
vault --version
```

### Compilando o fonte

```bash
sudo apt-get install -y make golang

# Criando a estrutura de diretórios:
mkdir -p $GOPATH/src/github.com/hashicorp && cd $_
git clone https://github.com/hashicorp/vault.git
cd vault
make bootstrap
make dev

#Agora que já tem o binário
mv vault /usr/local/bin/
vault --version
```

### Auto complete

Para ajudar nos comandos com o tab completando.

```bash
❯ vault -autocomplete-install
Usage: vault <command> [args]

Common commands:
    read        Read data and retrieves secrets
    write       Write data, configuration, and secrets
    delete      Delete secrets and configuration
    list        List data or secrets
    login       Authenticate locally
    agent       Start a Vault agent
    server      Start a Vault server
    status      Print seal and HA status
    unwrap      Unwrap a wrapped secret

Other commands:
    audit                Interact with audit devices
    auth                 Interact with auth methods
    debug                Runs the debug command
    kv                   Interact with Vault's Key-Value storage
    lease                Interact with leases
    license              Interact with licenses
    monitor              Stream log messages from a Vault server
    namespace            Interact with namespaces
    operator             Perform operator-specific tasks
    path-help            Retrieve API help for paths
    plugin               Interact with Vault plugins and catalog
    policy               Interact with policies
    print                Prints runtime configurations
    secrets              Interact with secrets engines
    ssh                  Initiate an SSH session
    token                Interact with tokens
    version-history      Prints the version history of the target Vault server
```

Nesse momento temos a cli do vault instalando, mas não temos um server para essa cli apontar, por isso o comando abaixo irá gerar erro o que é normal.

```bash
❯ vault status
Error checking seal status: Get "https://127.0.0.1:8200/v1/sys/seal-status": dial tcp 127.0.0.1:8200: connect: connection refused
```

# Conceitos Extras

## Backend Integrado

Na [documentação da hashicorp sobre storage backend](https://developer.hashicorp.com/vault/docs/configuration/storage/raft) é recomendado o uso do modelo integrado que fica guardado dentro de cada um dos nós do vault. Porém para que isso funcione é necessário utilizar em modo de alta disponibilidade com pelo menos 3 nodes, pois utiliza o protocolo [raft consensus](https://developer.hashicorp.com/vault/docs/internals/integrated-storage).

Abaixa uma tabela do número de servers tolerancia a falha.

| Servers | Quorum Size | Failure Tolerance |
|---|---|---|
| 1 | 1 | 0 |
| 2 | 2 | 0 |
| 3 | 2 | 1 |
| 4 | 3 | 1 |
| 5 | 3 | 2 |
| 6 | 4 | 2 |
| 7 | 4 | 3 |

Na documentação oficial do vault recomenda-se 5 servers para ter 2 falhas. Ou usa-se 3 server ou 5 servers. Para um estudo mais avançado sobre raft consensus veja o link <https://raft.github.io/>.

Antigamente era recomendado o uso do consul, mas depois da versão 1.4 essa recomendação mudou e tornou tudo mais simples.

| Armazenamento Integrado              | Armazenamento externo                                                                                                                |                                                                                                       |
|--------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------|
| Suporte HashiCorp                    | Sim                                                                                                                                  | Suporte limitado                                                                                      |
| Operação                             | Operacionalmente mais simples, sem necessidade de instalação de software adicional.                                                  | Deve instalar e configurar o ambiente de armazenamento externo fora do Vault.                         |
| rede                                 | Um salto de rede a menos.                                                                                                            | Salto de rede extra entre o Vault e o sistema de armazenamento externo (por exemplo, cluster Consul). |
| Solução de problemas e monitoramento | O armazenamento integrado faz parte do Vault; portanto, o Vault é o único sistema que você precisa monitorar e solucionar problemas. | A origem da falha pode ser o armazenamento externo                                                    |
| Localização dos dados                | Os dados criptografados do Vault são armazenados no mesmo host em que o processo do servidor Vault é executado.                      | O servidor Vault e o armazenamento de dados são hospedados em hosts fisicamente separados.            |
| requisitos de sistema                | SSDs devem ser usados para o disco rígido.                                                                                           | Siga os requisitos do sistema fornecidos pelo back-end de armazenamento escolhido.                    |             |

Nós iremos usar essa configuração dentro do /etc/vault/config.hcl quando instalar no kubernetes.

```json
storage "raft" {
  path = "/path/to/raft/data"
  node_id = "raft_node_1"
}
cluster_addr = "http://127.0.0.1:8201"
```

## Telemetria



